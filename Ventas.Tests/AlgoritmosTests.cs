﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using FluentAssertions;
using Ploeh.AutoFixture;
using Ventas.Comun.Combinatorios;
using Ventas.Comun.Impl;
using Ventas.Comun.Modelos;
using Ventas.Repositorio.Entidades;
using Xunit;

namespace Ventas.Tests
{
    public class AlgoritmosTests
    {
        private Fixture fixture;
        public AlgoritmosTests()
        {
            fixture = new Fixture();
            fixture.Behaviors.OfType<ThrowingRecursionBehavior>()
                .ToList().ForEach(b => fixture.Behaviors.Remove(b));

            fixture.Behaviors.Add(new OmitOnRecursionBehavior(1));
            
        }
        [Fact]
        public void combinatorio()
        {
            var lista = fixture.CreateMany<Direccion>(3).ToList();
            var com = new Combinations<Direccion>(lista,2);
            Console.WriteLine(com.Count);
        }

        [Fact]
        public void Mochila()
        {
            var carga = fixture.CreateMany<Venta>(100).ToList();
            var mochila = new Mochila();
            var total = 0M;
            var aCargar = mochila.CargaALLevar(carga, 7000, out total);
            aCargar.Sum(x => x.MontoTotal).Should().BeLessOrEqualTo(7000);

            Debug.WriteLine(total);
        }
    }
}