--CREATE TABLE Chofer (
--	DEscripcion char(50),
--	compute_0002 char(1),
--	compute_0003 char(3),
--	Documento char(20),
--	compute_0005 char(6),
--	compute_0006 datetime,
--	Estado char(1));
INSERT INTO Chofer VALUES (
	'ZENON RAMOS / TRANSP SANVIZ D6P-730               ',
	'',
	'DNI',
	'09126085            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'EUSEBIO PEREZ / F2U-249                           ',
	'',
	'DNI',
	'46829918            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'INACTIVO                                          ',
	'',
	'DNI',
	'09690734            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'EDDY MOLINA / RAUL PINTO  A3K-891                 ',
	'',
	'DNI',
	'09289060            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JONATHAN GALECIO / TRANSP G Y R - B4E-819         ',
	'',
	'DNI',
	'43761801            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'ELVIS SARE / P1Y-747                              ',
	'',
	'DNI',
	'40518802            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'FAVIO REYES / FRANCISCO PINTO C2R-880             ',
	'',
	'DNI',
	'10126264            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'GUILLERMO TAMARIZ / TRANSP AL  PQA-387            ',
	'',
	'DNI',
	'8292650             ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JOSE ZELA TORRES  QI-3026                         ',
	'',
	'DNI',
	'10650465            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JEAN CHAVEZ ARANA C2Q-886                         ',
	'',
	'DNI',
	'47560986            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'MARIO QUISPE  B7C-711                             ',
	'',
	'DNI',
	'09493954            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'ALEJANDRO VALDEON / TRANSP SANVIZ  DOE-782        ',
	'',
	'DNI',
	'46537404            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'MANUEL ABAD  / TRANSP SANVIZ DOE-893              ',
	'',
	'DNI',
	'46656707            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'JOSE HUAMAN / FRANCISCO PINTO C2R-908             ',
	'',
	'DNI',
	'09832962            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'MURILLO LABAN TEOFILO / TRANSP SANVIZ B0R-743     ',
	'',
	'DNI',
	'03892838            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'JULIO BOZA / EDU QG-6640                          ',
	'',
	'DNI',
	'42854335            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'CHRISTIAN SANCHEZ  / TRANSP PINTO B4P-867         ',
	'',
	'DNI',
	'10028552            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'CRISTIAN MARCELO H. / TRANSP SANVIZ  BOR-776      ',
	'',
	'DNI',
	'40253166            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'MARCOS FLORES / TRANSP SANVIZ  BOP-771            ',
	'',
	'DNI',
	'40532916            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'TRANSFERENCIA - OFICINA                           ',
	'',
	'DNI',
	'10707836            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'KELVIN QUIROZ - F4Z-728                           ',
	'',
	'DNI',
	'32131238            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'BRYAN VIDAL MATIENZO - FRANCISCO PINTO  C0L-935   ',
	'',
	'DNI',
	'41222873            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'LUIS ANGELES / TRANSP SANVIZ D6I-782              ',
	'',
	'DNI',
	'43988646            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'MARTIN LOPEZ - TRASNAMAR PIT-238                  ',
	'',
	'DNI',
	'10484870            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JUVENAL A. CAJAHUANCA  ADW-720                    ',
	'',
	'DNI',
	'8766376             ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'LUIS HERRERA / SV D9F-720                         ',
	'',
	'DNI',
	'06664480            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'JULIO GIRON / TRANSP SANVIZ  BOR-743              ',
	'',
	'DNI',
	'19823315            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'VICTOR NEGRETE REVOLLEDO / TRASNP SANVIZ BOP-771  ',
	'',
	'DNI',
	'72882257            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'6660942             ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'REYMI REYES LOPEZ / TRANSP SANVIZ BOR-743         ',
	'',
	'DNI',
	'03892838            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'EDY MOLINA - RAUL PINTO A3K-891                   ',
	'',
	'DNI',
	'41827344            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'10346585            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'10078284            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'12345678            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'6237210             ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'CARLOS MUNAYCO- TRANSP SANVIZ  D6L-743            ',
	'',
	'DNI',
	'46726892            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'EVARISTO CRUZ / C2R-849                           ',
	'',
	'DNI',
	'09578124            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'JULIO ENRIQUEZ / EDU F5M-702                      ',
	'',
	'DNI',
	'43686444            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'MATHFER SOP. LOGISTICO  PQV-378                   ',
	'',
	'DNI',
	'8349326             ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'ROMULO TINEO  QO-3208                             ',
	'',
	'DNI',
	'07910756            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'ANDRE ARTETA BARDALES - TRANSP RAUL PINTO  C7N-822',
	'',
	'DNI',
	'46750548            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'MARTIN LOPEZ - DOMINICA VASQUEZ A2F-943           ',
	'',
	'DNI',
	'22121212            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JORGE MERA - EDSON CAMPUSANO PIO-354              ',
	'',
	'DNI',
	'12345678            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'RAUL PINTO - TRANSP PINTO F9W-878                 ',
	'',
	'DNI',
	'42184431            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'GUIDO SALAS MAMANI  PIQ-420                       ',
	'',
	'DNI',
	'44551144            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'33536925            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'45646498            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'12345678            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JAIME E MENDOZA - TRANSP REF MENDOZA SAC  PIQ-705 ',
	'',
	'DNI',
	'41062691            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'OSCAR CABALLERO  / TRANSP SANVIZ  D6J-740         ',
	'',
	'DNI',
	'41703869            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'47474747            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'88888888            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JUAN SILVA VELASQUEZ - B6P-854                    ',
	'',
	'DNI',
	'09882951            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'RAUL SALAS / C7H-763                              ',
	'',
	'DNI',
	'08292650            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'VACANTE EKT 1                                     ',
	'',
	'DNI',
	'EKT1                ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'VACANTE EKT 2                                     ',
	'',
	'DNI',
	'EKT2                ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'XXXXXXXXX           ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'VASQUEZ  TRINIDAD ALEJANDRO                       ',
	'',
	'DNI',
	'15443321            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'EDGARD ALVARADO VASQUEZ PIF - 122                 ',
	'',
	'DNI',
	'45428638            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'CATHERINE BERNAL- RICHARD LI - C2R-909            ',
	'',
	'DNI',
	'09868027            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'VACANTE EDUSA I                                   ',
	'',
	'DNI',
	'11111111            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'VACANTE EDUSA II                                  ',
	'',
	'DNI',
	'22222222            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'HUGO CHAVEZ - ALDO VIZCARDO C2R-854               ',
	'',
	'DNI',
	'41241966            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'44444444            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'OFICINA                                           ',
	'',
	'DNI',
	'54555555            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'OFICINA II                                        ',
	'',
	'DNI',
	'11111111            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'EDDY MOLINA  - TRANSP PINTO C7N-822               ',
	'',
	'DNI',
	'22222222            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'DAVID MONTOYA - RICHARD LEE C2R-908               ',
	'',
	'DNI',
	'41539201            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'DIONICIO FELIX ANAMARIA - EVENTUAL A7J-911        ',
	'',
	'DNI',
	'O8908185            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'MARTIN LOPEZ VASQUEZ - EVENTUAL F0A-716           ',
	'',
	'DNI',
	'47706631            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JAVIER QUIROGA / TRANSP PINTO  C0L-934            ',
	'',
	'DNI',
	'41114792            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'HUGO HUAMANI LAPA / TRANSP SANVIZ  D9F-720        ',
	'',
	'DNI',
	'45042834            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'OFICINA III                                       ',
	'',
	'DNI',
	'88888888            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'FREDDY SALAS / TRANSP SANVIZ D6P-730              ',
	'',
	'DNI',
	'41012574            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'10101010            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'PERCY COLONIO A9D-898                             ',
	'',
	'DNI',
	'11111111            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JESUS ANAMARIA  A7J-911                           ',
	'',
	'DNI',
	'08829634            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'33333333            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'VACANTE II                                        ',
	'',
	'DNI',
	'44444444            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JUAN PABLO CHUMAN  -  TRANSP CHUMAN DPU-758       ',
	'',
	'DNI',
	'55555555            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'WADNER ORTIZ - TRANSP GUERRERO B4W-804            ',
	'',
	'DNI',
	'40242035            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'99999999            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JUAN MANCO - RICHARD LEE  C2R-907                 ',
	'',
	'DNI',
	'45604513            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'ROBERTO VILCA - RICHARD LEE- C2Q-887              ',
	'',
	'DNI',
	'41486456            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'MICKOL HUAMANI - FRANCISCO PINTO C2R-881          ',
	'',
	'DNI',
	'70293621            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'DIONICIO FELIX ANAMARIA A7J-911                   ',
	'',
	'DNI',
	'08908185            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'MARTIN LOPEZ VASQUEZ F0A-716                      ',
	'',
	'DNI',
	'47706631            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'MARCELINO ZELA - D2S-771                          ',
	'',
	'DNI',
	'80614460            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'ARCADIO PORTOLATINO - RAUL PINTO- C2R-846         ',
	'',
	'DNI',
	'08350103            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'HUGO CHAVEZ  C2R-880                              ',
	'',
	'DNI',
	'54645454            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'XXX                                               ',
	'',
	'DNI',
	'45454               ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'ORLANDO LOSSIO RGW-386                            ',
	'',
	'DNI',
	'45454               ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'JHONY MANRIQUE / TRANSP SANVIZ - B0P-771          ',
	'',
	'DNI',
	'10484870            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'FRANK AGUSTIN PECHO VEGA                          ',
	'',
	'DNI',
	'41521842            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'CARLOS MUNAYCO / SV B0P-771                       ',
	'',
	'DNI',
	'99999999            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'JESUS CASAS / QI-3124                             ',
	'',
	'DNI',
	'07910757            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'I');
INSERT INTO Chofer VALUES (
	'MARIO QUISPE / C2Q-886                            ',
	'',
	'DNI',
	'09493954            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
INSERT INTO Chofer VALUES (
	'OSCAR HUAYTALLA / D5C-339                         ',
	'',
	'DNI',
	'07528689            ',
	'777447',
	'2016-01-10 11:02:14.003',
	'A');
