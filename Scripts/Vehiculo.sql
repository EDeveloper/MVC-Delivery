--CREATE TABLE Vehiculo (
--	PlacaVehiculo char(10),
--	compute_0002 float,
--	compute_0003 char(6),
--	compute_0004 char(6),
--	compute_0005 float,
--	compute_0006 float,
--	compute_0007 char(9),
--	Estado char(1));
INSERT INTO Vehiculo VALUES (
	'A3K-891   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'A7J-911   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'A9D-898   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'B0P-771   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B0R-743   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B4E-819   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'B4P-867   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B4W-804   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B6P--854  ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'B6P-854   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B7C-711   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B7X-900   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'B8K-854   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'BOP-771   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'BOR-743   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'BOR-776   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2Q-886   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2Q-887   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-846   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-849   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-854   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-880   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-881   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-907   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-908   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C2R-909   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C7H-763   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'C7N-822   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'COL-934   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'COL-935   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'CQR-908   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'CR2-849   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'D0E-782   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D2S-771   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D3I-793   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D5C-339   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D6I-782   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D6J-740   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D6L-743   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D6P-730   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'D9F-720   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'DOE-893   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'EKT1      ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'EKT2      ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'F2S-945   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'F2U-249   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'F4Z-728   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'F5M-702   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'FOA-716   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'MMM       ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'OO-9393   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'P1Y-747   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'PGO-770   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PGR-043   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIA-195   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIF-122   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIK-710   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIM-795   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIO-354   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIP-567   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIQ-420   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIQ-705   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIR-733   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIS-367   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PIW-184   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PQB-277   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PQM-398   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PQT-146   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PQT-416   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'PQU029    ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PQV-378   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'PQW-773   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QF-3026   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QG-6640   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'QG-9797   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'QI-2200   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QI-3026   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QI-3742   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QI-4185   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QI-9131   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QI-9231   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QO-2970   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QO-3208   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'QO-3209   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'QO-3734   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'QQ-9207   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'A');
INSERT INTO Vehiculo VALUES (
	'QQ-9385   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'RIB-206   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'RIZ-060   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'RO-1163   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'XI-4380   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'XO-2489   ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'XXXXX     ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
INSERT INTO Vehiculo VALUES (
	'XXXXXX    ',
	1,
	'TOYOTA',
	'COROLA',
	250,
	1000,
	'828388393',
	'I');
