﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Mapeos
{
    public class MapeoConfiguracion
    {
        public static void Configura()
        {
            Mapper.CreateMap<Venta, VentaDto>()
            .ForMember(dest => dest.VentaId,
            map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Cliente, ClienteDto>()
            .ForMember(dest => dest.ClienteId,
            map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Vendedor, VendedorDto>()
            .ForMember(dest => dest.VendedorId,
            map => map.MapFrom(orig => orig.Id))
            .ForMember(d => d.Avatar, opt => opt.Ignore());


            Mapper.CreateMap<Direccion, DireccionDto>()
            .ForMember(dest => dest.DireccionId,
            map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<VentaDetalle, VentaDetalleDto>();

            Mapper.CreateMap<Chofer, ChoferDto>()
            .ForMember(dest => dest.ChoferId,
            map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Vehiculo, VehiculoDto>()
            .ForMember(dest => dest.VehiculoId,
            map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Papeleta, PapeletaDto>()
            .ForMember(dest => dest.PapeletaId,
            map => map.MapFrom(orig => orig.Id));

            Mapper.CreateMap<Zona, ZonaDto>()
            .ForMember(dest => dest.ZonaId,
            map => map.MapFrom(orig => orig.Id));

            //Mapper.CreateMap<Venta, DespachoDto>()
            //.ForMember(dest => dest.VentaId,
            //map => map.MapFrom(orig => orig.Id));

            //Mapper.CreateMap<VentaDetalle, DespachoDetalleDto>();
        }
    }
}
