﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class VehiculoDto
    {
        public int VehiculoId { get; set; }
        public string PlacaVehiculo { get; set; }
        public int ChoferId { get; set; }
        public string MarcaVehiculo { get; set; }
        public string Modelo { get; set; }
        public string Color { get; set; }
        public string Descripcion { get; set; }
        public Decimal Peso { get; set; }
        public Decimal Capacidad { get; set; }
        public string NumeroSoat { get; set; }
        public string Anio { get; set; }
        public string Motor { get; set; }
        public string Chasis { get; set; }
        public string Avatar { get; set; }
        public string Estado { get; set; }
        public string NombreCompleto { get { return PlacaVehiculo + " " + Descripcion + " Capacidad:" + Capacidad; } }
        //public IEnumerable<DireccionDto> Direcciones { get; set; }
    }
}
