﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class DireccionDto
    {
        public int DireccionId { get; set; }
        public int ClienteId { get; set; } //EF
        public int ZonaId { get; set; } //EF
        public int ClienteCodigo { get; set; }
        public int DireccionCodigo { get; set; }
        public string Descripcion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        public string GeoFlag { get; set; }
    }
}
