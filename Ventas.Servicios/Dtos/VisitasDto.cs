﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class VisitasDto
    {
        public int ClientId { get; set; }

        public string Nombre { get; set; }

        public int DireccionId { get; set; }

        public DireccionDto Direccion { get; set; }
    }
}
