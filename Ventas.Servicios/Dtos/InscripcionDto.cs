﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class InscripcionDto
    {
        public int EventoId { get; set; }

        public string Asistente { get; set; }

        public int Entradas { get; set; }
        //public string Direccion { get; set; }
        //public string Latitud { get; set; }
        //public string Longitud { get; set; }
        //public string Departamento { get; set; }
        //public string Provincia { get; set; }
        //public string Distrito { get; set; }

    }
}
