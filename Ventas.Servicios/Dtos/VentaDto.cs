﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class VentaDto
    {
        public int VentaId { get; set; }
        public int ClienteId { get; set; } //solo por que uso EF
        public int DireccionId { get; set; } //solo por que uso EF
        public int VendedorId { get; set; } //solo por que uso EF
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaDespacho { get; set; }
        public string FormadePago { get; set; }
        public string EstadoDespacho { get; set; }
        public string MonedaDocumento { get; set; }
        public decimal TipodeCambio { get; set; }
        public decimal MontoAfecto { get; set; }
        public decimal MontoNoAfecto { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal MontoTotal { get; set; }
        public string Estado { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        public IEnumerable<VentaDetalleDto> VentaDetalle { get; set; }
    }
}
