﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class ClienteDto
    {
        public int ClienteId { get; set; }
        public int ClienteCodigo { get; set; }
        public string TipoCliente { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string ClienteNombre { get; set; }
        public string Direccion { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Estado { get; set; }
        public Decimal RatioVenta { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        public IEnumerable<DireccionDto> Direcciones { get; set; }
    }
}
