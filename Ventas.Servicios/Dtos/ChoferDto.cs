﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class ChoferDto
    {
        public int ChoferId { get; set; }
        public string ChoferCodigo { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Brevete { get; set; }
        public string CategoriaBrevete { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string NombreCompleto { get { return Nombres + " " + Apellidos; } }

        //public IEnumerable<DireccionDto> Direcciones { get; set; }
    }
}
