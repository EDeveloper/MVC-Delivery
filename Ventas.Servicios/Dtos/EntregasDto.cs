﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Servicios.Dtos
{
    public class EntregasDto
    {
        public int VentaId { get; set; }

        public string Cliente { get; set; }

        public decimal MontoTotal { get; set; }

        public string Direccion { get; set; }
    }
}
