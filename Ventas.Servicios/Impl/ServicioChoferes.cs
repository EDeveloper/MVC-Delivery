﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioChoferes
    {
        private IOlimpoRepository contexto;

        public ServicioChoferes()
        {
            contexto = new EFOlimpoRepository();
        }
        public Dtos.ChoferDto Traer(int id)
        {
            var chofer = contexto
                .ChoferRepository.Find(e => e.Id == id)
                .ProjectTo<ChoferDto>().FirstOrDefault();
            return chofer;

        }

        public IEnumerable<Dtos.ChoferDto> BuscarChoferes(string busqueda)
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Chofer> choferes = contexto.ChoferRepository.GetAll();

            if (!string.IsNullOrEmpty(busqueda))
                choferes = choferes.Where(x => x.Nombres.Contains(busqueda));

            var lista = choferes.ProjectTo<ChoferDto>();
            //int row=lista.Count();
            return lista;
        }

    }
}
