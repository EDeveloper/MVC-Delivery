﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioVehiculos
    {
        private IOlimpoRepository contexto;

        public ServicioVehiculos()
        {
            contexto = new EFOlimpoRepository();
        }
        public Dtos.VehiculoDto Traer(int id)
        {
            var vehiculo = contexto
                .ChoferRepository.Find(e => e.Id == id)
                .ProjectTo<VehiculoDto>().FirstOrDefault();

            return vehiculo;

        }

        public IEnumerable<Dtos.VehiculoDto> BuscarVehiculos(string busqueda)
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Vehiculo> vehiculo = contexto.VehiculoRepository.GetAll();

            if (!string.IsNullOrEmpty(busqueda))
                vehiculo = vehiculo.Where(x => x.PlacaVehiculo.Contains(busqueda));

            var lista = vehiculo.ProjectTo<VehiculoDto>();
            //int row=lista.Count();
            return lista;
        }

    }
}
