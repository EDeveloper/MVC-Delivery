﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ventas.Servicios.Dtos;
using Ventas.Repositorio.Entidades;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;

namespace Ventas.Servicios.Impl
{
    public class ServicioBusqueda
    {
        private IOlimpoRepository contexto;
        public ServicioBusqueda()
        {
            contexto = new EFOlimpoRepository();
        }
        public IEnumerable<Dtos.EventoDto> Buscar(string busqueda)
        {
            // Ir al Repositorio de datos y buscar

            IQueryable<Evento> eventos = contexto.EventoRepository.GetAll();
            
            if (!string.IsNullOrEmpty(busqueda)) 
                eventos = eventos.Where(x=>x.Titulo.Contains(busqueda));

            var lista = eventos.ProjectTo<EventoDto>();
            return lista;
        }
    }
}
