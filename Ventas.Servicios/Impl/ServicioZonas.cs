﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioZonas
    {
        private IOlimpoRepository contexto;

        public ServicioZonas()
        {
            contexto = new EFOlimpoRepository();
        }
        
        public Dtos.ZonaDto Traer(int id)
        {
            var zona = contexto
                .ZonaRepository.Find(e => e.Id == id)
                .ProjectTo<ZonaDto>().FirstOrDefault();
            return zona;

        }

        public IEnumerable<Dtos.ZonaDto> BuscarZonas(string busqueda)
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Zona> zonas = contexto.ZonaRepository.GetAll();
            if (!string.IsNullOrEmpty(busqueda))
                zonas = zonas.Where(x => x.Descripcion.Contains(busqueda));

            var lista = zonas.ProjectTo<ZonaDto>();
            return lista;
        }

        //public void Inscribir(InscripcionDto request)
        //{
        //    var direccion = contexto
        //        .DireccionRepository
        //        .SingleOrDefault(x => x.Id == request.EventoId);
        //    //cliente.AddClienteDireccion(new Direccion() { Descripcion = request.Asistente });
        //    contexto.DireccionRepository.Update(direccion);
        //    contexto.Commit();
        //}

        public void Create(ZonaDto request)
        {
            Zona zona = new Zona();
            zona.Descripcion = request.Descripcion;
            zona.Departamento = request.Departamento;
            zona.Provincia = request.Provincia;
            zona.Distrito = request.Distrito;
            zona.ZonaCodigo = request.ZonaCodigo;
            //zona.Coordenadas = request.Coordenadas;
            zona.Estado = request.Estado;
            zona.UltimoUsuario = request.UltimoUsuario;
            zona.UltimaFechaModif = request.UltimaFechaModif;
            contexto.ZonaRepository.Add(zona);
            contexto.Commit();
        }

        public void Update(ZonaDto request)
        {
            var zona = contexto
                .ZonaRepository
                .SingleOrDefault(x => x.Id == request.ZonaId);

            zona.Descripcion = request.Descripcion;
            zona.Departamento = request.Departamento;
            zona.Provincia = request.Provincia;
            zona.Distrito = request.Distrito;
            zona.ZonaCodigo = request.ZonaCodigo;
            //zona.Coordenadas = request.Coordenadas;
            zona.Estado = request.Estado;
            zona.UltimoUsuario = request.UltimoUsuario;
            zona.UltimaFechaModif = request.UltimaFechaModif;
            contexto.ZonaRepository.Update(zona);
            contexto.Commit();
        }

        public void Delete(ZonaDto request)
        {
            var zona = contexto
                .ZonaRepository
                .SingleOrDefault(x => x.Id == request.ZonaId );
            contexto.ZonaRepository.Delete(zona);
            contexto.Commit();
        }
    }
}
