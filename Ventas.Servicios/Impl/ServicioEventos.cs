﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioEventos
    {
        private IOlimpoRepository contexto;

        public ServicioEventos()
        {
            contexto = new EFOlimpoRepository();
        }
        public Dtos.EventoDto Traer(int id)
        {
            //return eventos.FirstOrDefault(e => e.EventoId == id);
            var evento = contexto
                .EventoRepository.Find(e => e.Id == id)
                .ProjectTo<EventoDto>().FirstOrDefault();
            return evento;

            //return eventos.Find(e => e.EventoId == id);
        }

        public void Inscribir(InscripcionDto request)
        {
            var evento = contexto
                .EventoRepository
                .SingleOrDefault(x => x.Id == request.EventoId);
            evento.AddAsistente(new Asistente() { Nombre = request.Asistente });
            contexto.EventoRepository.Update(evento);
            contexto.Commit();
        }
    }
}
