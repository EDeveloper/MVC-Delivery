﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ventas.Servicios.Dtos;
using Ventas.Repositorio.Entidades;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;

namespace Ventas.Servicios.Impl
{
    public class ServicioClienteBusqueda
    {
        private IOlimpoRepository contexto;
        public ServicioClienteBusqueda()
        {
            contexto = new EFOlimpoRepository();
        }

        public Dtos.DireccionDto TraerDireccion(int id)
        {
            var direccion = contexto
                .DireccionRepository.Find(e => e.Id == id)
                .ProjectTo<DireccionDto>().FirstOrDefault();
            return direccion;

        }

        public IEnumerable<Dtos.DireccionDto> TraerDirecciones(string busqueda, string Geo)
        {

            IQueryable<Direccion> direcciones = contexto.DireccionRepository.GetAll().OrderByDescending(x => x.GeoFlag);

            if (!string.IsNullOrEmpty(busqueda))
                direcciones = direcciones.Where(x => x.Descripcion.Contains(busqueda));


            //if (Geo == "S") //Direcciones geolocalizadas
            //{
            direcciones = direcciones.Where(x => x.GeoFlag == Geo);
            //}
            //else //Direccion NO geolocalizadas
            //{
            //    direcciones = direcciones.Where(x => x.Latitud == null);
            //}

            var lista = direcciones.ProjectTo<DireccionDto>();
            return lista;


        }

        public void UpdateDireccion(DireccionDto request)
        {
            var direccion = contexto
                .DireccionRepository
                .SingleOrDefault(x => x.Id == request.DireccionId);

            //direccion.D = request.DireccionId;
            direccion.Descripcion = request.Descripcion;
            direccion.Departamento = request.Departamento;
            direccion.Provincia = request.Provincia;
            direccion.Distrito = request.Distrito;
            direccion.Estado = request.Estado;
            direccion.Latitud = request.Latitud;
            direccion.Longitud = request.Longitud;
            direccion.ClienteId = request.ClienteId;
            contexto.DireccionRepository.Update(direccion);
            contexto.Commit();
        }

        public IEnumerable<Dtos.ClienteDto> Buscar(string codigo, string documento, string busqueda)
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Cliente> clientes = contexto.ClienteRepository.GetAll();

            if (!string.IsNullOrEmpty(codigo))
            {
                int codigoVendedor = Int32.Parse(codigo);
                clientes = clientes.Where(x => x.ClienteCodigo == codigoVendedor);
            }
            if (!string.IsNullOrEmpty(documento))
                clientes = clientes.Where(x => x.NumeroDocumento.Contains(documento));

            if (!string.IsNullOrEmpty(busqueda))
                clientes = clientes.Where(x => x.ClienteNombre.Contains(busqueda));

            var lista = clientes.ProjectTo<ClienteDto>();
            return lista;
        }

        public IEnumerable<Dtos.GeolocalizacionDto> BuscarGeo(string codigo, string documento, string busqueda, string tipo)
        {
            //List<Direccion> direcciones = contexto.DireccionRepository.GetAll().Where(x => x.GeoFlag == tipo).ToList();
            //Direccion direccion = direcciones.GetAll();

            //var totalClientes = contexto
            //.ClienteRepository
            //.GetAll().Where(m => BuscarxCodigo(m, codigo))
            //.Select(x => new
            //{
            //    Id = x.Id,
            //    Codigo = x.ClienteCodigo,
            //    Nombre = x.ClienteNombre,
            //    Direccion = x.Direcciones.FirstOrDefault(y => y.ClienteId == direccion.ClienteId),
            //})
            //.Where(z => z.Direccion != null && z.Direccion.GeoFlag == tipo)
            //.ToList();

            //List<GeolocalizacionDto> ClienteLista = new List<GeolocalizacionDto>();
            Int32 clienteCodigo = 0;
            if (!string.IsNullOrEmpty(codigo))
            {
                clienteCodigo = Int32.Parse(codigo);
            }

            var direccionesGeo = (from c in contexto.ClienteRepository.GetAll()
                                  join d in contexto.DireccionRepository.GetAll() on c.Id equals d.ClienteId
                                  //where d.GeoFlag == tipo && ( d.ClienteCodigo == clienteCodigo || 0 == clienteCodigo )
                                  where d.GeoFlag == tipo
                                  select new GeolocalizacionDto
                                  {
                                      ClienteId = c.Id,
                                      ClienteNombre = c.ClienteNombre,
                                      TipoDocumento = c.TipoDocumento,
                                      NumeroDocumento = c.NumeroDocumento,
                                      DireccionId = d.Id,
                                      Direccion = d.Descripcion,
                                      Estado = c.Estado,
                                      Latitud = d.Latitud,
                                      Longitud = d.Longitud,
                                      Departamento = d.Departamento,
                                      Provincia = d.Provincia,
                                      Distrito = d.Distrito,
                                      CodigoCliente = c.ClienteCodigo,
                                      CodigoDireccion = d.DireccionCodigo
                                  }).ToList();

            //direccionesGeo.Where(x => BuscarxCodigo(x, codigo) == true);
            //direccionesGeo.Where(x => BuscarxDocumento(x, documento) == true);
            //direccionesGeo.Where(x => BuscarxNombre(x, busqueda) == true);
            //return direccionesGeo.ToList();
            return direccionesGeo.Where(x => BuscarxCodigo(x, codigo) && BuscarxDocumento(x, documento) && BuscarxNombre(x, busqueda)).ToList();
        }

        private bool BuscarxCodigo(GeolocalizacionDto cliente, string codigo)
        {
            if (string.IsNullOrEmpty(codigo)) return true;
            int codigoCliente = Int32.Parse(codigo);
            if (cliente.CodigoCliente == codigoCliente) return true;
            return false;
        }
        private bool BuscarxDocumento(GeolocalizacionDto cliente, string documento)
        {
            if (string.IsNullOrEmpty(documento)) return true;
            if (cliente.NumeroDocumento.Contains(documento)) return true;
            return false;
        }
        private bool BuscarxNombre(GeolocalizacionDto cliente, string busqueda)
        {
            if (string.IsNullOrEmpty(busqueda)) return true;
            if (cliente.ClienteNombre.Contains(busqueda)) return true;
            return false;
        }
    }
}
