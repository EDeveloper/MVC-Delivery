﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioVentas
    {
        private IOlimpoRepository contexto;

        public ServicioVentas()
        {
            contexto = new EFOlimpoRepository();
        }
        public Dtos.VentaDto Traer(int id)
        {
            var venta = contexto
                .VentaRepository.Find(e => e.Id == id)
                .ProjectTo<VentaDto>().FirstOrDefault();

            return venta;
        }

        public Dtos.DespachoDto TraerDespacho(int id)
        {

            var venta = (from v in contexto.VentaRepository.GetAll()
                          join c in contexto.ClienteRepository.GetAll() on v.ClienteId equals c.Id
                          join d in contexto.DireccionRepository.GetAll() on v.DireccionId equals d.Id
                         where v.Id == id
                         select new DespachoDto
                          {
                              VentaId = v.Id,
                              ClienteId = v.ClienteId,
                              DireccionId = v.DireccionId,
                              VendedorId = v.VendedorId,
                              TipoDocumento = v.TipoDocumento,
                              NumeroDocumento = v.NumeroDocumento,
                              FechaDocumento = v.FechaDocumento,
                              DireccionEntrega = d.Descripcion,
                              ClienteEntrega = c.ClienteNombre,
                              FechaDespacho = v.FechaDespacho,
                              FormadePago = v.FormadePago,
                              EstadoDespacho = v.EstadoDespacho,
                              MonedaDocumento = v.MonedaDocumento,
                              TipodeCambio = v.TipodeCambio,
                              MontoAfecto = v.MontoAfecto,
                              MontoNoAfecto = v.MontoNoAfecto,
                              MontoImpuesto = v.MontoImpuesto,
                              MontoTotal = v.MontoTotal,
                              Estado = v.Estado,
                              Latitud = d.Latitud,
                              Longitud = d.Longitud,
                              UltimoUsuario = v.UltimoUsuario,
                              UltimaFechaModif = v.UltimaFechaModif
                          }).FirstOrDefault();

            return venta;
        }

        //public IEnumerable<Dtos.VentaDto> Buscar(string busqueda)
        //{
        //    // Ir al Repositorio de datos y buscar
        //    IQueryable<Venta> ventas = contexto.VentaRepository.GetAll();
        //    if (!string.IsNullOrEmpty(busqueda))
        //        ventas = ventas.Where(x => x.ClienteNombre.Contains(busqueda));

        //    var lista = ventas.ProjectTo<VentaDto>();
        //    return lista;
        //}

        public void Create(VentaDto request)
        {
            Venta venta = new Venta();
            //Venta.VentaNombre = request.VentaNombre;
            //Venta.Direccion = request.Direccion;
            //Venta.TipoDocumento = request.TipoDocumento;
            //Venta.NumeroDocumento = request.NumeroDocumento;
            //Venta.Telefono = request.Telefono;
            //Venta.Latitud = request.Latitud;
            //Venta.Longitud = request.Longitud;
            //Venta.CorreoElectronico = request.CorreoElectronico;
            //Venta.Estado = request.Estado;
            contexto.VentaRepository.Add(venta);
            contexto.Commit();
        }
        public void Update(VentaDto request)
        {
            var venta = contexto
                .VentaRepository
                .SingleOrDefault(x => x.Id == request.VentaId);

            //venta.ClienteDireccion = request.ClienteDireccion;
            venta.EstadoDespacho = request.EstadoDespacho;
            venta.TipoDocumento = request.TipoDocumento;
            venta.NumeroDocumento = request.NumeroDocumento;
            venta.FechaDocumento = request.FechaDocumento;
            //venta.Estado = request.EstadoVenta;
            //venta.Latitud = request.Latitud;
            //venta.Longitud = request.Longitud;
            //venta.ClienteDireccion = request.ClienteDireccion;
            venta.MontoAfecto = request.MontoAfecto;
            venta.MontoImpuesto = request.MontoImpuesto;
            venta.MontoTotal = request.MontoTotal;
            //venta.Chofer = request.Chofer;
            //venta.Vehiculo = request.Vehiculo;

            contexto.VentaRepository.Update(venta);
            contexto.Commit();
        }
        public void Delete(VentaDto request)
        {
            var venta = contexto
                .VentaRepository
                .SingleOrDefault(x => x.Id == request.VentaId);
            contexto.VentaRepository.Delete(venta);
            contexto.Commit();
        }
    }
}
