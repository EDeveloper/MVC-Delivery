﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioPapeletas
    {
        private IOlimpoRepository contexto;

        public ServicioPapeletas()
        {
            contexto = new EFOlimpoRepository();
        }
        public Dtos.PapeletaDto Traer(int id)
        {
            var papeleta = contexto
                .PapeletaRepository.Find(e => e.Id == id)
                .ProjectTo<PapeletaDto>().FirstOrDefault();
            return papeleta;

        }

        public IEnumerable<Dtos.PapeletaDto> BuscarPapeletas(string ChoferCodigo, string PlacaVehiculo, string busqueda, string TipoInfraccion)
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Papeleta> papeletas = contexto.PapeletaRepository.GetAll();

            //if (!string.IsNullOrEmpty(busqueda))
            //    papeletas = papeletas.Where(x => x.Nombres.Contains(busqueda));

            //if (ChoferId > 0)
            //    papeletas = papeletas.Where(x => x.Nombres.Contains(busqueda));

            //if (!string.IsNullOrEmpty(ChoferCodigo))
            //    papeletas = papeletas.Where(x => x.ChoferCodigo == ChoferCodigo);


            //if (!string.IsNullOrEmpty(PlacaVehiculo))
            //    papeletas = papeletas.Where(x => x.PlacaVehiculo == PlacaVehiculo);

            //if (!string.IsNullOrEmpty(TipoInfraccion))
            //    papeletas = papeletas.Where(x => x.TipoInfraccion == TipoInfraccion);

            var lista = papeletas.ProjectTo<PapeletaDto>();
            //int row=lista.Count();
            return lista;
        }

        public void Create(PapeletaDto request)
        {
            Papeleta papeleta = new Papeleta();
            //papeleta.ClienteNombre = request.ClienteNombre;
            //papeleta.TipoDocumento = request.TipoDocumento;
            //papeleta.NumeroDocumento = request.NumeroDocumento;
            //papeleta.Nombres = request.Nombres;
            //papeleta.Apellidos = request.Apellidos;
            //papeleta.PlacaVehiculo = request.PlacaVehiculo;
            //papeleta.Direccion = request.Direccion;
            papeleta.Estado = request.Estado;
            papeleta.Distrito = request.Distrito;
            //papeleta.Brevete = request.Brevete;
            papeleta.FechaPapeleta = request.FechaPapeleta;
            contexto.PapeletaRepository.Add(papeleta);
            contexto.Commit();
        }
        public void Update(PapeletaDto request)
        {
            var papeleta = contexto
                .PapeletaRepository
                .SingleOrDefault(x => x.Id == request.PapeletaId);

            //papeleta.ClienteNombre = request.ClienteNombre;
            //papeleta.Direccion = request.Direccion;
            //papeleta.TipoDocumento = request.TipoDocumento;
            //papeleta.NumeroDocumento = request.NumeroDocumento;
            //papeleta.Telefono = request.Telefono;
            //papeleta.Latitud = request.Latitud;
            //papeleta.Longitud = request.Longitud;
            //papeleta.CorreoElectronico = request.CorreoElectronico;
            papeleta.Estado = request.Estado;
            contexto.PapeletaRepository.Update(papeleta);
            contexto.Commit();
        }

        public void Delete(PapeletaDto request)
        {
            //var cliente = contexto
            //    .ClienteRepository
            //    .SingleOrDefault(x => x.Id == request.ClienteId);

            var papeleta = contexto
                .PapeletaRepository
                .SingleOrDefault(x => x.Id == request.PapeletaId);
            contexto.PapeletaRepository.Delete(papeleta);
            contexto.Commit();
        }

        public IEnumerable<Dtos.VehiculoDto> PapeletaVehiculo()
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Vehiculo> vehiculos = contexto.VehiculoRepository.GetAll();

            //if (!string.IsNullOrEmpty(busqueda))
            vehiculos = vehiculos.Where(x => x.Estado == "A");

            var lista = vehiculos.ProjectTo<VehiculoDto>();
            //int row=lista.Count();
            return lista;
        }

        public IEnumerable<Dtos.ChoferDto> PapeletaChofer()
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Chofer> choferes = contexto.ChoferRepository.GetAll();

            //if (!string.IsNullOrEmpty(busqueda))
            choferes = choferes.Where(x => x.Estado == "A");

            var lista = choferes.ProjectTo<ChoferDto>();
            //int row=lista.Count();
            return lista;
        }

    }
}
