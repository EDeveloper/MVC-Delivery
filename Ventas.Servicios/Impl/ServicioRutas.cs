﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Ventas.Comun.Impl;
using Ventas.Comun.Modelos;
using Ventas.Comun.Viajero;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioRutas
    {
        private readonly IOlimpoRepository context;
        private Rutas algoritmos;
        public ServicioRutas()
        {
            context = new EFOlimpoRepository();
            algoritmos = new Rutas();
        }

        public List<Dtos.EntregasDto> RutaEntregas(int vehiculoSeleccionado, DateTime fechaEntrega)
        {
            var from = fechaEntrega.Date;
            var to = fechaEntrega.AddDays(1).Date;
            var totalEntregas = context
                .VentaRepository
                .GetAll()
                .Where(x => x.FechaDespacho >= from && x.FechaDespacho <= to
                  && x.EstadoDespacho == "S"
                && x.VentaDireccion.GeoFlag == "S")
                .ToList();

            var vehiculo = context.VehiculoRepository.First(x => x.Id == vehiculoSeleccionado);

            var aEntregar = algoritmos.Entregas(totalEntregas,Convert.ToInt32(vehiculo.Capacidad));

            return aEntregar.Select(x => new EntregasDto
            {
                VentaId = x.Id,
                Cliente = x.VentaCliente.ClienteNombre,
                MontoTotal = x.MontoTotal,
                Direccion = x.VentaCliente.Direccion
            }).ToList();
        }

        public List<Dtos.VisitasDto> RutaVisitas(int vendedorId, DateTime fechaVisita)
        {
            DayOfWeek dia = fechaVisita.DayOfWeek;
            //Vendedor vendedor = context.VendedorRepository.First(v => v.Id == vendedorId);
            List<VendedorZona> zonas = context.VendedorZonaRepository.GetAll().Where(x=>x.VendedorId==vendedorId).ToList();

            VendedorZona zona = zonas.FirstOrDefault(x=>ZonaAVisitar(x, dia));
            if (zona == null) return new List<VisitasDto>();
            
            
            var totalVisitas = context
                .ClienteRepository
                .GetAll()
                .OrderByDescending(x => x.RatioVenta) // aqui se toman los que tienen el ratio mas alto
                // se necesita saber cuantos pueden visitar
                // se necesita un indicador o algo parecido para poder saber si ya fueron visitados
                .Select(x => new
                {
                    Id = x.Id,
                    Codigo = x.ClienteCodigo,
                    Nombre = x.ClienteNombre,
                    Direccion = x.Direcciones.FirstOrDefault(y => y.DireccionZona.Id == zona.Id),
                })
                .Where(z => z.Direccion != null && z.Direccion.GeoFlag == "S")
                .ToList();

            var visitas = totalVisitas.Select(y => new Visitas
            {
                Id = y.Id,
                Codigo = y.Codigo,
                Nombre = y.Nombre,
                Direccion = y.Direccion,
                DireccionId = y.Direccion.Id,
                Latitud = Convert.ToDouble(y.Direccion.Latitud),
                Longitud = Convert.ToDouble(y.Direccion.Longitud)
            }).ToList();


            var aEntregar = algoritmos.Entregas(visitas);

            return aEntregar.Select(x => new VisitasDto
            {
                ClientId = x.Id,
                Nombre = x.Nombre,
                DireccionId = x.DireccionId,
                Direccion = Mapper.Map<Direccion, DireccionDto>(x.Direccion)
            }).ToList();
        }

        private bool ZonaAVisitar(VendedorZona zona, DayOfWeek dia)
        {
            if (zona.DomingoFlag=="S" && dia == DayOfWeek.Sunday) return true;
            if (zona.LunesFlag == "S" && dia == DayOfWeek.Monday) return true;
            if (zona.MartesFlag == "S" && dia == DayOfWeek.Thursday) return true;
            if (zona.MiercolesFlag == "S" && dia == DayOfWeek.Wednesday) return true;
            if (zona.JuevesFlag == "S" && dia == DayOfWeek.Tuesday) return true;
            if (zona.ViernesFlag == "S" && dia == DayOfWeek.Friday) return true;
            if (zona.SabadoFlag == "S" && dia == DayOfWeek.Saturday) return true;
            return false;
        }

        public IEnumerable<ChoferDto> Choferes()
        {
            return Mapper.Map<IList<Chofer>, IList<ChoferDto>>(context.ChoferRepository.Find(e => e.Estado == "A").ToList());
        }

        public IEnumerable<VehiculoDto> Vehiculos()
        {
            return Mapper.Map<IList<Vehiculo>, IList<VehiculoDto>>(context.VehiculoRepository.Find(e => e.Estado == "A").ToList());
        }

        public void GrabaRutaEntrega(List<EntregasDto> rutas, int VehiculoId, DateTime fechaDespacho)
        {
            var vehiculo = context.VehiculoRepository.Find(e => e.Id == VehiculoId).FirstOrDefault();
            var choferId = vehiculo.ChoferId;
            var choferCodigo = vehiculo.ChoferxDefecto.ChoferCodigo;

            DateTime date = DateTime.Now;
            //DateTime date2 = date.AddDays(1);
            DateTime date2 = fechaDespacho;
            
            int dia = (int)date2.DayOfWeek;
            string rutaCodigo = dia.ToString() + "" + choferCodigo;
            int contador = 0;
            var ruta = new Ruta()
            {
                VehiculoId = VehiculoId,
                ChoferId = choferId,
                CodigoRuta = rutaCodigo,
                FechaRegistro = date,
                FechaDespacho = date2,
                MontoTotal = 1,
                Observaciones = "",
                Estado = "PR",
                KilometrajeInicial = 1,
                KilometrajeFinal = 1,
                UltimoUsuario = "NAVARROE",
                UltimaFechaModif = date,
            };
            foreach (var ventas in rutas.ToList())
            {
                contador ++;
                ruta.AddRutaDetalle(new RutaDetalle()
                {
                    VentaId = ventas.VentaId,
                    Estado = "PR",
                    UltimoUsuario = "NAVARROE",
                    UltimaFechaModif = date,
                    RutaSecuencia = contador,
                });

                var venta = context
                .VentaRepository
                .SingleOrDefault(x => x.Id == ventas.VentaId);
                //cliente.AddClienteDireccion(new Direccion() { Descripcion = request.Asistente });
                venta.EstadoDespacho = "N";
                context.VentaRepository.Update(venta);

            }

            //Ruta ruta = new Ruta();
            context.RutaRepository.Add(ruta);
            context.Commit();

        }

        public IEnumerable<VendedorDto> Vendedores()
        {
            return Mapper.Map<IList<Vendedor>, IList<VendedorDto>>(context.VendedorRepository.Find(e => e.Estado == "A").ToList());
        }

        public IEnumerable<ZonaDto> Zonas()
        {
            return Mapper.Map<IList<Zona>, IList<ZonaDto>>(context.ZonaRepository.Find(e => e.Estado == "A").ToList());
        }

        public void GrabaRutaVisita(List<VisitasDto> visitas, int vendedorId)
        {
            DateTime date = DateTime.Now;
            var visita = new Visita()
            {
                VendedorId = vendedorId,
                CodigoVisita = "",
                FechaRegistro = date,
                FechaVisita = date,
                Observaciones = "",
                Estado = "PR",
                UltimoUsuario = "NAVARROE",
                UltimaFechaModif = date,
            };
            foreach (var clientes in visitas.ToList())
            {
                visita.AddVisitaDetalle(new VisitaDetalle()
                {
                    ClienteId = clientes.ClientId,
                    DireccionId = clientes.DireccionId,
                    Estado = "PR",
                    UltimoUsuario = "NAVARROE",
                    UltimaFechaModif = date
                });
            }

            //Ruta ruta = new Ruta();
            context.VisitaRepository.Add(visita);
            context.Commit();
        }
    }
}
