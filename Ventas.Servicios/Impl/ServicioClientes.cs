﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;

namespace Ventas.Servicios.Impl
{
    public class ServicioClientes
    {
        private IOlimpoRepository contexto;

        public ServicioClientes()
        {
            contexto = new EFOlimpoRepository();
        }
        
        public Dtos.ClienteDto Traer(int id)
        {
            var cliente = contexto
                .ClienteRepository.Find(e => e.Id == id)
                .ProjectTo<ClienteDto>().FirstOrDefault();
            return cliente;

        }

        public Dtos.DireccionDto TraerDireccion(int id)
        {
            var direccion = contexto
                .DireccionRepository.Find(e => e.Id == id)
                .ProjectTo<DireccionDto>().FirstOrDefault();
            return direccion;
        }

        public Dtos.VentaDto TraerDespacho(string date)
        {
            DateTime dt = Convert.ToDateTime(date);

            var despacho = contexto
                .VentaRepository.Find(e => e.FechaDocumento == dt)
                .ProjectTo<VentaDto>().FirstOrDefault();

            return despacho;

        }

        //public IEnumerable<Dtos.ClienteDto> Buscar(string busqueda)
        //{
        //    // Ir al Repositorio de datos y buscar
        //    IQueryable<Cliente> clientes = contexto.ClienteRepository.GetAll();
        //    if (!string.IsNullOrEmpty(busqueda))
        //        clientes = clientes.Where(x => x.ClienteNombre.Contains(busqueda));

        //    var lista = clientes.ProjectTo<ClienteDto>();
        //    return lista;
        //}

        public void Geolocalizar(GeolocalizacionDto request)
        {
            var direccion = contexto
                .DireccionRepository
                .SingleOrDefault(x => x.Id == request.DireccionId);
            //cliente.AddClienteDireccion(new Direccion() { Descripcion = request.Asistente });
            direccion.Descripcion = request.Direccion;
            direccion.Latitud = request.Latitud;
            direccion.Longitud = request.Longitud;
            direccion.Departamento = request.Departamento;
            direccion.Distrito = request.Distrito;
            direccion.Provincia = request.Provincia;
            direccion.GeoFlag = request.GeoFlag;
            contexto.DireccionRepository.Update(direccion);
            contexto.Commit();
        }

        public void Inscribir(InscripcionDto request)
        {
            var direccion = contexto
                .DireccionRepository
                .SingleOrDefault(x => x.Id == request.EventoId);
            //cliente.AddClienteDireccion(new Direccion() { Descripcion = request.Asistente });
            contexto.DireccionRepository.Update(direccion);
            contexto.Commit();
        }

        public void Create(ClienteDto request)
        {
            Cliente cliente = new Cliente();
            cliente.ClienteNombre = request.ClienteNombre;
            cliente.Direccion = request.Direccion;
            cliente.TipoDocumento = request.TipoDocumento;
            cliente.NumeroDocumento = request.NumeroDocumento;
            cliente.Telefono = request.Telefono;
            //cliente.Latitud = request.Latitud;
            //cliente.Longitud = request.Longitud;
            cliente.CorreoElectronico = request.CorreoElectronico;
            cliente.Estado = request.Estado;
            contexto.ClienteRepository.Add(cliente);
            contexto.Commit();
        }
        
        public void Update(ClienteDto request)
        {
            var cliente = contexto
                .ClienteRepository
                .SingleOrDefault(x => x.Id == request.ClienteId);

            cliente.ClienteNombre = request.ClienteNombre;
            cliente.Direccion = request.Direccion;
            cliente.TipoDocumento = request.TipoDocumento;
            cliente.NumeroDocumento = request.NumeroDocumento;
            cliente.Telefono = request.Telefono;
            //cliente.Latitud = request.Latitud;
            //cliente.Longitud = request.Longitud;
            cliente.CorreoElectronico = request.CorreoElectronico;
            cliente.Estado = request.Estado;
            contexto.ClienteRepository.Update(cliente);
            contexto.Commit();
        }

        public void Delete(ClienteDto request)
        {
            //var cliente = contexto
            //    .ClienteRepository
            //    .SingleOrDefault(x => x.Id == request.ClienteId);

            var cliente = contexto
                .ClienteRepository
                .SingleOrDefault(x => x.Id == request.ClienteId );
            contexto.ClienteRepository.Delete(cliente);
            contexto.Commit();
        }
    }
}
