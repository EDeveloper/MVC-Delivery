﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ventas.Servicios.Dtos;
using Ventas.Repositorio.Entidades;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;

namespace Ventas.Servicios.Impl
{
    public class ServicioVentaBusqueda
    {
        private IOlimpoRepository contexto;
        public ServicioVentaBusqueda()
        {
            contexto = new EFOlimpoRepository();
        }
        public IEnumerable<Dtos.VentaDto> Buscar(string busqueda)
        {
            // Ir al Repositorio de datos y buscar
            DateTime dt = Convert.ToDateTime(busqueda);
            IQueryable<Venta> ventas = contexto.VentaRepository.GetAll();
            //var dato1 = ventas.Count();
            if (!string.IsNullOrEmpty(busqueda))
                ventas = ventas.Where(x => x.FechaDespacho >= dt);

            var lista = ventas.ProjectTo<VentaDto>();
            //var dato2 = lista.Count();
            return lista;
        }

        public IEnumerable<Dtos.DespachoDto> BuscarDespachos(string busqueda)
        {
            DateTime dt = Convert.ToDateTime(busqueda);
            var dfrom = dt.Date;
            var dto = dt.AddDays(1).Date;

            var ventas = (from v in contexto.VentaRepository.GetAll()
                          join c in contexto.ClienteRepository.GetAll() on v.ClienteId equals c.Id
                          join d in contexto.DireccionRepository.GetAll() on v.DireccionId equals d.Id
                          join r in contexto.RutaDetalleRepository.GetAll() on v.Id equals r.VentaId
                          join rd in contexto.RutaRepository.GetAll() on r.RutaId equals rd.Id
                          where (rd.FechaDespacho >= dfrom && rd.FechaDespacho < dto) && d.GeoFlag == "S"
                          orderby r.Id
                          select new DespachoDto
                          {
                              VentaId = v.Id,
                              ClienteId = v.ClienteId,
                              DireccionId = v.DireccionId,
                              VendedorId = v.VendedorId,
                              TipoDocumento = v.TipoDocumento,
                              NumeroDocumento = v.NumeroDocumento,
                              FechaDocumento = v.FechaDocumento,
                              DireccionEntrega = d.Descripcion,
                              ClienteEntrega = c.ClienteNombre,
                              FechaDespacho = v.FechaDespacho,
                              FormadePago = v.FormadePago,
                              EstadoDespacho = v.EstadoDespacho,
                              MonedaDocumento = v.MonedaDocumento,
                              TipodeCambio = v.TipodeCambio,
                              MontoAfecto = v.MontoAfecto,
                              MontoNoAfecto = v.MontoNoAfecto,
                              MontoImpuesto = v.MontoImpuesto,
                              MontoTotal = v.MontoTotal,
                              Estado = v.Estado,
                              Latitud = d.Latitud,
                              Longitud = d.Longitud,
                              UltimoUsuario = v.UltimoUsuario,
                              UltimaFechaModif = v.UltimaFechaModif
                          }).ToList();

            return ventas;
        }
    }
}
