﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper.QueryableExtensions;
using Ventas.Repositorio.DDDContext;
using Ventas.Repositorio.Entidades;
using Ventas.Servicios.Dtos;
//using System.Web;
using System.IO;
using System.Web;

namespace Ventas.Servicios.Impl
{
    public class ServicioVendedores
    {
        private IOlimpoRepository contexto;

        public ServicioVendedores()
        {
            contexto = new EFOlimpoRepository();
        }

        public Dtos.VendedorDto Traer(int id)
        {
            var usuario = contexto
                .VendedorRepository
                .Single(x => x.Id == id);

            var usuarioDto = Mapper.Map<Vendedor, VendedorDto>(usuario);
            if (!string.IsNullOrEmpty(usuario.Avatar))
            {
                var ruta = HttpContext.Current.Server.MapPath(@"~\AvatarVendedor");
                usuarioDto.Avatar = new Imagen
                {
                    Nombre = usuario.Avatar,
                    Contenido = File.ReadAllBytes(Path.Combine(ruta, usuario.Avatar))
                };

            }
            return usuarioDto;
        }

        public IEnumerable<Dtos.VendedorDto> BuscarVendedores(string busqueda)
        {
            // Ir al Repositorio de datos y buscar
            IQueryable<Vendedor> vendedores = contexto.VendedorRepository.GetAll();

            if (!string.IsNullOrEmpty(busqueda))
                vendedores = vendedores.Where(x => x.Nombres.Contains(busqueda));


            var lista = vendedores.ProjectTo<VendedorDto>();
            return lista;
        }

        public void Update(VendedorDto request)
        {
            var vendedor = contexto
            .VendedorRepository
            .SingleOrDefault(x => x.Id == request.VendedorId);


            if (request.Avatar != null)
            {
                // grabar el avatar en la carpeta Avatars 
                vendedor.Avatar = "avatar" + vendedor.Id + Path.GetExtension(request.Avatar.Nombre);
                var ruta = HttpContext.Current.Server.MapPath(@"~\AvatarVendedor");
                File.WriteAllBytes(Path.Combine(ruta, vendedor.Avatar), request.Avatar.Contenido);
            }

            vendedor.TipoVendedor = request.TipoVendedor;
            vendedor.Nombres = request.Nombres;
            vendedor.Apellidos = request.Apellidos;
            vendedor.TipoDocumento = request.TipoDocumento;
            vendedor.NumeroDocumento = request.NumeroDocumento;
            vendedor.CorreoElectronico = request.CorreoElectronico;
            vendedor.Telefono = request.Telefono;
            vendedor.Direccion = request.Direccion;
            vendedor.EquipoVenta = request.EquipoVenta;
            vendedor.FuerzaVenta = request.FuerzaVenta;
            vendedor.Supervisor = request.Supervisor;
            vendedor.Estado = request.Estado;
            vendedor.UltimoUsuario = request.UltimoUsuario;
            //vendedor.UltimaFechaModif = request.UltimaFechaModif;
            contexto.VendedorRepository.Update(vendedor);
            contexto.Commit();

        }

    }
}
