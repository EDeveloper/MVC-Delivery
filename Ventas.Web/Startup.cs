﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ventas.Web.Startup))]
namespace Ventas.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
