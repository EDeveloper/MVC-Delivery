﻿var controlador = (function () {
    //alert("OK");
    function initialize(lat, lng, address) {
        //alert("OK2");
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            title: address
        });

        marker.setMap(map);

        var infotext = address + '<hr>' +
                       'Latitude: ' + lat + '<br>Longitude: ' + lng;
        var infowindow = new google.maps.InfoWindow();
        infowindow.setContent(infotext);
        infowindow.setPosition(new google.maps.LatLng(lat, lng));
        infowindow.open(map);
    }

    function mostrarMap() {
        alert("x");
        var address = "Av El Sol 560 - Santiago de Surco";
        var geocoder = new google.maps.Geocoder();
        
        geocoder.geocode({ 'address': address }, function (results, status) {
            alert(results);
            alert(status);
            if (status == google.maps.GeocoderStatus.OK) {
                alert(address);
                var longaddress = results[0].address_components[0].long_name;
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                document.getElementById("Latitud").value = latitude;
                document.getElementById("Longitud").value = longitude;
                initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);
            } else {
                alert('Geocode error: ' + status);
            }
        });
    }

    return {
        mostrar: mostrarMap,
        initialize: initialize
    }
})();

var app = (function () {
    function registrarEventos() {
        $('#marker-button').on('click', controlador.mostrar);
    };

    //publicar la interface de modulo
    return {
        iniciar: function () {
            registrarEventos();
            //controlador.mostrar();
        }
    };
})();

$(function () {
    app.iniciar();
});


