﻿alert("Llegamos");
$(function () {
    $("#ListGrid").jqGrid({
        url: "/ Vendedores/BuscarVendedores",
        datatype: 'json',
        mtype: 'Get',
        colNames: ['Id', 'Nombres', 'Apellidos', 'TipoVendedor', 'Estado'],
        colModel: [
            { key: true, hidden: true, name: 'Id', index: 'Id', editable: true },
            { key: false, name: 'Nombres', index: 'Nombres', editable: true },
            { key: false, name: 'Apellidos', index: 'Apellidos', editable: true },
            /*{ key: false, name: 'TargetDate', index: 'TargetDate', editable: true, formatter: 'date', formatoptions: { newformat: 'd/m/Y' } },*/
            { key: false, name: 'TipoVendedor', index: 'TipoVendedor', editable: true, edittype: 'select', editoptions: { value: { 'MA': 'Mayorista', 'ME': 'Mercados', 'BO': 'Boticas' } } },
            { key: false, name: 'Estado', index: 'Estado', editable: true, edittype: 'select', editoptions: { value: { 'A': 'Activo', 'I': 'Inactivo' } } }],
        pager: jQuery('#pager'),
        rowNum: 10,
        rowList: [10, 20, 30, 40],
        height: '100%',
        viewrecords: true,
        caption: 'Todo List',
        emptyrecords: 'No records to display',
        jsonReader: {
            root: "rows",
            page: "page",
            total: "total",
            records: "records",
            repeatitems: false,
            Id: "0"
        },
        autowidth: true,
        multiselect: false
    })
        //.navGrid('#pager', { edit: true, add: true, del: true, search: false, refresh: true },
        //{
        //    // edit options
        //    zIndex: 100,
        //    url: '/TodoList/Edit',
        //    closeOnEscape: true,
        //    closeAfterEdit: true,
        //    recreateForm: true,
        //    afterComplete: function (response) {
        //        if (response.responseText) {
        //            alert(response.responseText);
        //        }
        //    }
        //},
        //{
        //    // add options
        //    zIndex: 100,
        //    url: "/TodoList/Create",
        //    closeOnEscape: true,
        //    closeAfterAdd: true,
        //    afterComplete: function (response) {
        //        if (response.responseText) {
        //            alert(response.responseText);
        //        }
        //    }
        //},
        //{
        //    // delete options
        //    zIndex: 100,
        //    url: "/TodoList/Delete",
        //    closeOnEscape: true,
        //    closeAfterDelete: true,
        //    recreateForm: true,
        //    msg: "Are you sure you want to delete this task?",
        //    afterComplete: function (response) {
        //        if (response.responseText) {
        //            alert(response.responseText);
        //        }
        //    }
        //});
});