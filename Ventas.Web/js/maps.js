﻿var controlador = (function () {

    function initialize(lat, lng, address) {
        var mapOptions = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("myMap"),
        mapOptions);

        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            title: address
        });

        marker.setMap(map);

        var infotext = address + '<hr>' +
                       'Latitude: ' + lat + '<br>Longitude: ' + lng;
        var infowindow = new google.maps.InfoWindow();
        infowindow.setContent(infotext);
        infowindow.setPosition(new google.maps.LatLng(lat, lng));
        infowindow.open(map);
    }

    function mostrarMap() {
        alert("Prueb")
        var address = document.getElementById("Direccion").value;
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var longaddress = results[0].address_components[0].long_name;
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
                document.getElementById("Latitud").value = latitude;
                document.getElementById("Longitud").value = longitude;
                initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);
            } else {
                alert('Geocode error: ' + status);
            }
        });
    }

    return {
        mostrar: mostrarMap,
        initialize: initialize
    }
})();

var app = (function () {
    function registrarEventos() {
        $('#marker-button').on('click', controlador.mostrar);
    };

    //publicar la interface de modulo
    return {
        iniciar: function () {
            registrarEventos();
            //controlador.mostrar();
        }
    };
})();

$(function () {
    app.iniciar();
});


