﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ventas.Servicios.Dtos;
using Ventas.Web.Models;

namespace Ventas.Web.Mapeos
{
    public class MapeoConfiguracion
    {
        public static void Configuracion()
        {
            Mapper.CreateMap<VentaDto, ListaVentasViewModel>();
            Mapper.CreateMap<VentaDto, VentaDetalleViewModel>();
            Mapper.CreateMap<VentaDetalleDto, VentaDetalle>();

            //Despachos
            Mapper.CreateMap<VentaDto, ListaDespachosViewModel>();
            Mapper.CreateMap<VentaDto, BuscaDespachosViewModel>();

            Mapper.CreateMap<ClienteDto, ListaClientesViewModel>();
            Mapper.CreateMap<ClienteDto, ClienteDetalleViewModel>();
            Mapper.CreateMap<DireccionDto, ClienteDireccion>();

            Mapper.CreateMap<GeolocalizacionDto, ClientesGeoListModel>();
            Mapper.CreateMap<DespachoDto, ListaDespachosViewModel>();

            Mapper.CreateMap<ClienteDto, VentaCliente>();


            //Mapper.CreateMap<VendedorDto, VendedorListModel>();
            //Mapper.CreateMap<VendedorDto, VendedorEditModel>();
            //Mapper.CreateMap<VendedorEditModel, VendedorDto>();

            Mapper.CreateMap<VendedorDto, VendedorEditModel>();
            Mapper.CreateMap<VendedorEditModel, VendedorDto>();

            Mapper.CreateMap<VendedorDto, VendedorListModel>();
            Mapper.CreateMap<VendedorDto, VendedorViewModel>();

            Mapper.CreateMap<VehiculoDto, ListaVehiculosViewModel>();

            Mapper.CreateMap<ChoferDto, ChoferesListModel>();

            Mapper.CreateMap<DireccionDto, ListaLocationsViewModel>();

            Mapper.CreateMap<PapeletaDto, PapeletasListModel>();
            Mapper.CreateMap<PapeletaDto, PapeletasEditModel>();
            Mapper.CreateMap<PapeletaDto, PapeletasSearchModel>();

            Mapper.CreateMap<ZonaDto, ZonasListModel>();
            Mapper.CreateMap<ZonaDto, ZonasEditModel>();
        }
    }
}