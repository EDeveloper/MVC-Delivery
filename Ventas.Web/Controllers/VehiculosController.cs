﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Web.Mvc;
using AutoMapper;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;


//using System.Collections.Generic;
//using System.Linq;
//using System.Web;


//using System.Net;
//using System.Xml.Linq;
//using System.IO;
//using NPOI.HSSF.UserModel;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class VehiculosController : Controller
    {
        private ServicioVehiculos _servicioVehiculos;
        public VehiculosController()
        {
            _servicioVehiculos = new ServicioVehiculos();
        }

        public ActionResult Index()
        {
            return View(new BuscaVehiculosViewModel());
        }


        public PartialViewResult BuscarVehiculos(string busqueda)
        {
            //busqueda = "e";
            IEnumerable<VehiculoDto> vehiculo;

            if (string.IsNullOrEmpty(busqueda))
                vehiculo = new List<VehiculoDto>();
            else
                vehiculo = _servicioVehiculos.BuscarVehiculos(busqueda);

            return PartialView("_VehiculosEncontrados", Mapper.Map<IEnumerable<VehiculoDto>, IList<ListaVehiculosViewModel>>(vehiculo));

        }

        public ActionResult Reporte(string id)
        {
            id = "e";
            IEnumerable<VehiculoDto> choferes;

            if (string.IsNullOrEmpty(id))
                choferes = new List<VehiculoDto>();
            else
                choferes = _servicioVehiculos.BuscarVehiculos(id);

            var archivoExcel = new ReporteSimple().Exportar(id, choferes.ToList());

            return File(archivoExcel, "application/vnd.ms-excel", "reporte_" + id + ".xls");
        }

        public class ReporteSimple
        {
            public byte[] Exportar(string BusquedaAnterior, List<VehiculoDto> vehiculos)
            {
                //logger.InfoFormat("Se exporto la busqueda de: {0}", BusquedaAnterior);

                // codigo del Reporte
                int numReg = vehiculos.Count();
                //usar esto cuanto tienes un template
                //var nombreArchivo = NombreArchivo(); //@"d:\plantillas\balance.xlt";
                //var fs = new FileStream(nombreArchivo, FileMode.CreateNew);
                //var libro = new HSSFWorkbook(fs, true);

                var libro = new HSSFWorkbook();

                var hoja = libro.CreateSheet("Ejemplo Reporte");

                var fila = hoja.CreateRow(0);
                var celda = fila.CreateCell(0);
                celda.SetCellValue("Lista de Vendedores");
                //celda.CellStyle.WrapText = true;

                fila = hoja.CreateRow(1);
                celda = fila.CreateCell(0);
                celda.SetCellValue("Código");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(1);
                celda.SetCellValue("Tipo Vendedor");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(2);
                celda.SetCellValue("Nombes");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(3);
                celda.SetCellValue("Documento");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(4);
                celda.SetCellValue("Estado");
                //celda.CellStyle.WrapText = true;

                int count = 1;
                foreach (var vehiculo in vehiculos.ToList())
                {
                    count += 1;
                    fila = hoja.CreateRow(count);

                    celda = fila.CreateCell(0);
                    celda.SetCellValue(vehiculo.VehiculoId);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(1);
                    celda.SetCellValue(vehiculo.Descripcion );
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(2);
                    celda.SetCellValue(vehiculo.PlacaVehiculo);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(3);
                    celda.SetCellValue(vehiculo.NumeroSoat);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(4);
                    celda.SetCellValue(vehiculo.Estado);
                    //celda.CellStyle.WrapText = true;
                }

                //var celda2 = fila1.CreateCell(1);
                //celda2.SetCellValue("10/01/2016");

                var mstream = new MemoryStream();
                libro.Write(mstream);
                return mstream.ToArray();

                //return View();

            }
            private string NombreArchivo()
            {
                var ruta = Path.GetTempFileName();
                System.IO.File.Delete(ruta);
                return Path.ChangeExtension(ruta, "xls");
            }
        }

    }
}