﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;

namespace Ventas.Web.Controllers
{
    public class VisitasController : Controller
    {

        private ServicioRutas servicioRutas;
        
        public VisitasController()
        {
            servicioRutas = new ServicioRutas();
        }
        // GET: Rutas
        public ActionResult Inicio()
        {
            return View(InicializaModelo());
        }

        private VisitasViewModel InicializaModelo()
        {
            var modelo = new VisitasViewModel(servicioRutas.Vendedores(), servicioRutas.Zonas())
            {
                Rutas = new List<VisitasDto>()
            };
            return modelo;
        }

        [HttpPost]
        public PartialViewResult BuscaRutas(VisitasGuardarViewModel entrega)
        {
            var rutas = servicioRutas.RutaVisitas(entrega.VendedorSeleccionado, DateTime.Now);
            if (rutas.Count <= 0) @TempData["Success"] = "No se encontraron visitas";
            var modelo = new VisitasViewModel(servicioRutas.Vendedores(), servicioRutas.Zonas())
            {
                Rutas = rutas,
                ZonaSeleccionada = entrega.ZonaSeleccionada,
                VendedorSeleccionado = entrega.VendedorSeleccionado
            };
            
            return PartialView("_GuardaRutas",modelo);

        }
     

        [HttpPost]
        public ActionResult Guardar(VisitasGuardarViewModel entrega)
        {
            try
            {
                servicioRutas.GrabaRutaVisita(entrega.Rutas, entrega.VendedorSeleccionado);

                var modelo = new VisitasViewModel(servicioRutas.Vendedores(), servicioRutas.Zonas())
                {
                    Rutas = entrega.Rutas,
                    ZonaSeleccionada = entrega.ZonaSeleccionada,
                    VendedorSeleccionado = entrega.VendedorSeleccionado
                };
                TempData["Success"] = "Se genero la VISITA correctamente";
                return RedirectToAction("Inicio", modelo);

            }
            catch (Exception ex)
            {
                var modelo = new VisitasViewModel(servicioRutas.Vendedores(), servicioRutas.Zonas())
                {
                    Rutas = entrega.Rutas,
                    ZonaSeleccionada = entrega.ZonaSeleccionada,
                    VendedorSeleccionado = entrega.VendedorSeleccionado
                };
                TempData["Success"] = "Error al generar la visita" + ex.ToString();
                return View("Inicio",modelo);
            }
        }

    }
}