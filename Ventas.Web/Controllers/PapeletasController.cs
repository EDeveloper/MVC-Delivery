﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Web.Mvc;
using AutoMapper;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;


//using System.Collections.Generic;
//using System.Linq;
//using System.Web;


//using System.Net;
//using System.Xml.Linq;
//using System.IO;
//using NPOI.HSSF.UserModel;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class PapeletasController : Controller
    {
        private ServicioPapeletas _servicioPapeletas;
        public PapeletasController()
        {
            _servicioPapeletas = new ServicioPapeletas();
        }

        public ActionResult Index()
        {
            var model = new PapeletasSearchModel()
           {
               Choferes = new SelectList(_servicioPapeletas.PapeletaChofer(), "ChoferCodigo", "Nombres"),
               Vehiculos = new SelectList(_servicioPapeletas.PapeletaVehiculo(), "PlacaVehiculo", "Descripcion")
               //Infracciones = new SelectList(
               //                    new List<object>
               //                        {
               //                        new { value = "L" , text = "Leve"  },
               //                        new { value = "G" , text = "Grave"  },
               //                        new { value = "M" , text = "Muy Grave"  }
               //                        }, "value", "text"
               //                  )
           };
            return View(model);
        }


        public PartialViewResult BuscarPapeletas(string ChoferCodigo, string PlacaVehiculo)
        {
            //busqueda = "e";
            IEnumerable<PapeletaDto> papeletas;
            //int ChoferId = 1; string PlacaVehiculo = "zz"; string TipoInfraccion = "LV";
            //if (string.IsNullOrEmpty(busqueda))
            //    papeletas = new List<PapeletaDto>();
            //else
            papeletas = _servicioPapeletas.BuscarPapeletas(ChoferCodigo, PlacaVehiculo, PlacaVehiculo, PlacaVehiculo);

            return PartialView("_PapeletasEncontrados", Mapper.Map<IEnumerable<PapeletaDto>, IList<PapeletasListModel>>(papeletas));

        }
        [HttpGet]
        public ActionResult Create()
        {
            var model = new PapeletasEditModel()
            {
                //Person = new Person(),
                Vehiculos = new SelectList(_servicioPapeletas.PapeletaVehiculo(), "PlacaVehiculo", "Descripcion"),
                Choferes = new SelectList(_servicioPapeletas.PapeletaChofer(), "ChoferId", "Nombres")
                //Categorias = new SelectList(
                //                    new List<object>
                //                        {
                //                        new { value = "1" , text = "Clase A Categoría I"  },
                //                        new { value = "2" , text = "Clase A Categoría II-A"  },
                //                        new { value = "3" , text = "Clase A Categoría II-B"  },
                //                        new { value = "1" , text = "Clase A Categoría III-A"  },
                //                        new { value = "1" , text = "Clase A Categoría III-B"  },
                //                        new { value = "3" , text = "Clase A Categoría III-C"  }
                //                        },"value", "text"
                //                  ),
                //Infracciones = new SelectList(
                //                    new List<object>
                //                        {
                //                        new { value = "L" , text = "Leve"  },
                //                        new { value = "G" , text = "Grabe"  },
                //                        new { value = "M" , text = "Muy Grabe"  }
                //                        },"value", "text"
                //                  )


            };
            return View(model);
            //return PartialView("_Create", model);

        }

        //public ActionResult Create([Bind(Include = "LastName, FirstMidName, EnrollmentDate")]Student student)
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PapeletasEditModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PapeletaDto papeleta = new PapeletaDto();
                    //papeleta.ClienteId = model.ClienteId;
                    papeleta.TipoDocumento = model.TipoDocumento;
                    papeleta.NumeroDocumento = model.NumeroDocumento;
                    papeleta.Nombres = model.Nombres;
                    papeleta.Apellidos = model.Apellidos;
                    papeleta.PlacaVehiculo = model.PlacaVehiculo;
                    papeleta.Direccion = model.Direccion;
                    papeleta.Estado = model.Estado;
                    papeleta.Distrito = model.Distrito;
                    papeleta.Brevete = model.Brevete;
                    papeleta.FechaPapeleta = model.FechaPapeleta;
                    _servicioPapeletas.Create(papeleta);
                }
            }
            catch (Exception /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            PapeletaDto papeleta = _servicioPapeletas.Traer(id);
            //TempData["Success"]
            //ViewBag.Success = "Se Geolocalizo correctamente la dirección";
            return View(
                Mapper.Map<PapeletaDto, PapeletasEditModel>(papeleta)
                );
            //return PartialView("_Edit", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PapeletasEditModel model, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    PapeletaDto papeleta = _servicioPapeletas.Traer(id);
                    //papeleta.ClienteId = model.ClienteId;
                    papeleta.TipoDocumento = model.TipoDocumento;
                    papeleta.NumeroDocumento = model.NumeroDocumento;
                    //papeleta.ClienteNombre = model.ClienteNombre;
                    papeleta.Direccion = model.Direccion;
                    //papeleta.Telefono = model.Telefono;
                    //papeleta.CorreoElectronico = model.CorreoElectronico;
                    papeleta.Estado = model.Estado;
                    //papeleta.Latitud = model.Latitud;
                    //papeleta.Longitud = model.Longitud;
                    _servicioPapeletas.Update(papeleta);
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    return RedirectToAction("Ver", "Clientes", new { id = model.PapeletaId });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
            return RedirectToAction("Index", "BuscarClientes", new { id = model.PapeletaId });

        }


        public ActionResult Reporte(string id)
        {
            id = "e";
            IEnumerable<PapeletaDto> papeletas;

            if (string.IsNullOrEmpty(id))
                papeletas = new List<PapeletaDto>();
            else
                papeletas = new List<PapeletaDto>();
            //papeletas = _servicioPapeletas.BuscarPapeletas(id);

            var archivoExcel = ""; //new ReporteSimple().Exportar(id, papeletas.ToList());

            return File(archivoExcel, "application/vnd.ms-excel", "reporte_" + id + ".xls");
        }

        public class ReporteSimple
        {
            public byte[] Exportar(string BusquedaAnterior, List<ChoferDto> vehiculos)
            {
                //logger.InfoFormat("Se exporto la busqueda de: {0}", BusquedaAnterior);

                // codigo del Reporte
                int numReg = vehiculos.Count();
                //usar esto cuanto tienes un template
                //var nombreArchivo = NombreArchivo(); //@"d:\plantillas\balance.xlt";
                //var fs = new FileStream(nombreArchivo, FileMode.CreateNew);
                //var libro = new HSSFWorkbook(fs, true);

                var libro = new HSSFWorkbook();

                var hoja = libro.CreateSheet("Ejemplo Reporte");

                var fila = hoja.CreateRow(0);
                var celda = fila.CreateCell(0);
                celda.SetCellValue("Lista de Vendedores");
                //celda.CellStyle.WrapText = true;

                fila = hoja.CreateRow(1);
                celda = fila.CreateCell(0);
                celda.SetCellValue("Código");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(1);
                celda.SetCellValue("Tipo Vendedor");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(2);
                celda.SetCellValue("Nombes");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(3);
                celda.SetCellValue("Documento");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(4);
                celda.SetCellValue("Estado");
                //celda.CellStyle.WrapText = true;

                int count = 1;
                foreach (var vehiculo in vehiculos.ToList())
                {
                    count += 1;
                    fila = hoja.CreateRow(count);

                    celda = fila.CreateCell(0);
                    celda.SetCellValue(vehiculo.ChoferId);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(1);
                    celda.SetCellValue(vehiculo.Apellidos);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(2);
                    celda.SetCellValue(vehiculo.Nombres);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(3);
                    celda.SetCellValue(vehiculo.NumeroDocumento);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(4);
                    celda.SetCellValue(vehiculo.Estado);
                    //celda.CellStyle.WrapText = true;
                }

                //var celda2 = fila1.CreateCell(1);
                //celda2.SetCellValue("10/01/2016");

                var mstream = new MemoryStream();
                libro.Write(mstream);
                return mstream.ToArray();

                //return View();

            }
            private string NombreArchivo()
            {
                var ruta = Path.GetTempFileName();
                System.IO.File.Delete(ruta);
                return Path.ChangeExtension(ruta, "xls");
            }
        }

    }
}