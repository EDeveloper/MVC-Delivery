﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Web.Mvc;
using AutoMapper;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;


//using System.Collections.Generic;
//using System.Linq;
//using System.Web;


//using System.Net;
//using System.Xml.Linq;
//using System.IO;
//using NPOI.HSSF.UserModel;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class ChoferesController : Controller
    {
        private ServicioChoferes _servicioChoferes;
        public ChoferesController()
        {
            _servicioChoferes = new ServicioChoferes();
        }

        public ActionResult Index()
        {
            return View(new ChoferesSearchModel());
        }


        public PartialViewResult BuscarChoferes(string busqueda)
        {
            busqueda = "e";
            IEnumerable<ChoferDto> chofer;

            if (string.IsNullOrEmpty(busqueda))
                chofer = new List<ChoferDto>();
            else
                chofer = _servicioChoferes.BuscarChoferes(busqueda);

            return PartialView("_ChoferesEncontrados", Mapper.Map<IEnumerable<ChoferDto>, IList<ChoferesListModel>>(chofer));

        }

        public ActionResult Create()
        {
            var model = new ChoferesEditModel();
            return View(model);
            //return PartialView("_Create", model);
        }

        public ActionResult Reporte(string id)
        {
            id = "e";
            IEnumerable<ChoferDto> choferes;

            if (string.IsNullOrEmpty(id))
                choferes = new List<ChoferDto>();
            else
                choferes = _servicioChoferes.BuscarChoferes(id);

            var archivoExcel = new ReporteSimple().Exportar(id, choferes.ToList());

            return File(archivoExcel, "application/vnd.ms-excel", "reporte_" + id + ".xls");
        }

        public class ReporteSimple
        {
            public byte[] Exportar(string BusquedaAnterior, List<ChoferDto> vehiculos)
            {
                //logger.InfoFormat("Se exporto la busqueda de: {0}", BusquedaAnterior);

                // codigo del Reporte
                int numReg = vehiculos.Count();
                //usar esto cuanto tienes un template
                //var nombreArchivo = NombreArchivo(); //@"d:\plantillas\balance.xlt";
                //var fs = new FileStream(nombreArchivo, FileMode.CreateNew);
                //var libro = new HSSFWorkbook(fs, true);

                var libro = new HSSFWorkbook();

                var hoja = libro.CreateSheet("Ejemplo Reporte");

                var fila = hoja.CreateRow(0);
                var celda = fila.CreateCell(0);
                celda.SetCellValue("Lista de Vendedores");
                //celda.CellStyle.WrapText = true;

                fila = hoja.CreateRow(1);
                celda = fila.CreateCell(0);
                celda.SetCellValue("Código");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(1);
                celda.SetCellValue("Tipo Vendedor");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(2);
                celda.SetCellValue("Nombes");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(3);
                celda.SetCellValue("Documento");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(4);
                celda.SetCellValue("Estado");
                //celda.CellStyle.WrapText = true;

                int count = 1;
                foreach (var vehiculo in vehiculos.ToList())
                {
                    count += 1;
                    fila = hoja.CreateRow(count);

                    celda = fila.CreateCell(0);
                    celda.SetCellValue(vehiculo.ChoferId);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(1);
                    celda.SetCellValue(vehiculo.Apellidos );
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(2);
                    celda.SetCellValue(vehiculo.Nombres);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(3);
                    celda.SetCellValue(vehiculo.NumeroDocumento);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(4);
                    celda.SetCellValue(vehiculo.Estado);
                    //celda.CellStyle.WrapText = true;
                }

                //var celda2 = fila1.CreateCell(1);
                //celda2.SetCellValue("10/01/2016");

                var mstream = new MemoryStream();
                libro.Write(mstream);
                return mstream.ToArray();

                //return View();

            }
            private string NombreArchivo()
            {
                var ruta = Path.GetTempFileName();
                System.IO.File.Delete(ruta);
                return Path.ChangeExtension(ruta, "xls");
            }
        }

    }
}