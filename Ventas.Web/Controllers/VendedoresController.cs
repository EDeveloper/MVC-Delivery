﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Web.Mvc;
using AutoMapper;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;


namespace Ventas.Web.Controllers
{
    [Authorize]
    public class VendedoresController : Controller
    {
        private ServicioVendedores _servicioVendedores;
        public VendedoresController()
        {
            _servicioVendedores = new ServicioVendedores();
        }

        public ActionResult Index()
        {
            return View(new VendedorSearchModel() { TipoBusqueda = "I" });
        }


        public PartialViewResult BuscarVendedores(string tipoBusqueda, string vendedorCodigo, string busqueda)
        {
            IEnumerable<VendedorDto> vendedores;
            if (string.IsNullOrEmpty(vendedorCodigo) && string.IsNullOrEmpty(busqueda))
            {
                vendedores = new List<VendedorDto>();
                @TempData["Success"] = "Ingrese un criterio de busqueda";
            }
            else
            {
                vendedores = _servicioVendedores.BuscarVendedores(busqueda);
                if (vendedores.Count() <= 0) @TempData["Success"] = "No se encontraron vendedores";
            }

            //var lista = Mapper.Map<IEnumerable<VendedorDto>, IList<VendedorListModel>>(vendedores);

            //foreach (var vendedor in lista.ToList())
            //{
            //    if (!String.IsNullOrEmpty(vendedor.Avatar))
            //    {
            //        //var usuarioDto = Mapper.Map<Vendedor, VendedorDto>(vendedor);
            //        var ruta = HttpContext.Current.Server.MapPath(@"~\AvatarVendedor");
            //        vendedor.AvatarIMG = new Imagen
            //        {
            //            Nombre = vendedor.Avatar,
            //            Contenido = File.ReadAllBytes(Path.Combine(ruta, vendedor.Avatar))
            //        };
            //    }
            //}

            if (tipoBusqueda == "B")
            {
                //Retorna la vista parcial para la busqueda de vendedores
                return PartialView("_VendedoresBusqueda", Mapper.Map<IEnumerable<VendedorDto>, IList<VendedorListModel>>(vendedores));
            }
            else
            {
                //Retorna la vista parcial para la busqueda de vendedores
                return PartialView("_VendedoresEncontrados", Mapper.Map<IEnumerable<VendedorDto>, IList<VendedorListModel>>(vendedores));
            }
        }

        public PartialViewResult Seleccionar()
        {
            //var modelo = new VendedorSearchModel()
            //{
            //    TipoBusqueda = "S"
            //};
            //return modelo;

            return PartialView("_Seleccionar",
                    new VendedorSearchModel() { TipoBusqueda = "B" }
                    );
        }
        public PartialViewResult Edit(int id)
        {
            VendedorDto vendedor = _servicioVendedores.Traer(id);

            return PartialView("_Edit", Mapper.Map<VendedorDto, VendedorEditModel>(vendedor));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VendedorEditModel model, HttpPostedFileBase archivo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //public ActionResult Edit(VendedorEditModel model, HttpPostedFileBase archivo)
                    var vendedorDto = Mapper.Map<VendedorEditModel, VendedorDto>(model);

                    if (archivo != null && archivo.ContentLength > 0)
                    {
                        using (var lector = new BinaryReader(archivo.InputStream))
                        {
                            vendedorDto.Avatar = new Imagen()
                            {
                                Nombre = archivo.FileName,
                                Contenido = lector.ReadBytes(archivo.ContentLength)
                            };
                        }
                    }
                    _servicioVendedores.Update(vendedorDto);

                    //_servicioVendedores.Update(new VendedorDto()
                    //{
                    //    TipoVendedor = model.TipoVendedor,
                    //    Nombres = model.Nombres,
                    //    Apellidos = model.Apellidos,
                    //    TipoDocumento = model.TipoDocumento,
                    //    NumeroDocumento = model.NumeroDocumento,
                    //    CorreoElectronico = model.CorreoElectronico,
                    //    Telefono = model.Telefono,
                    //    Direccion = model.Direccion,
                    //    EquipoVenta = model.EquipoVenta,
                    //    FuerzaVenta = model.FuerzaVenta,
                    //    Supervisor = model.Supervisor,
                    //    Estado = model.Estado,
                    //    UltimoUsuario = model.UltimoUsuario,
                    //    UltimaFechaModif = model.UltimaFechaModif
                    //});
                }
                //ViewBag.Success = "Se Geolocalizo correctamente la dirección";
                TempData["Success"] = "Vendedor actualizado correctamente";
                return RedirectToAction("Browse", "Vendedor", new { id = model.VendedorId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Aviso", "Hubo un Error: " + ex);
                //return RedirectToAction("Edit", "Vendedor", new { id = model.VendedorId });

                return RedirectToAction("_Edit", model);

                //throw;
            }

        }

        //public PartialViewResult Browse(int id)
        //{
        //    var vendedor = _servicioVendedores.Traer(id);

        //    return PartialView("_Browse", Mapper.Map<VendedorDto, VendedorViewModel>(vendedor));
        //}

        public ActionResult Browse(int id)
        {
            VendedorDto vendedor = _servicioVendedores.Traer(id);
            return View(
                Mapper.Map<VendedorDto, VendedorViewModel>(vendedor)
                );

            //var vendedor = _servicioVendedores.Traer(id);

            //return PartialView("_Browse", Mapper.Map<VendedorDto, VendedorViewModel>(vendedor));
        }

        public ActionResult Reporte(string id)
        {
            id = "a";
            IEnumerable<VendedorDto> vendedores;
            if (string.IsNullOrEmpty(id))
                vendedores = new List<VendedorDto>();
            else
                vendedores = _servicioVendedores.BuscarVendedores(id);

            var archivoExcel = new ReporteSimple().Exportar(id, vendedores.ToList());

            return File(archivoExcel, "application/vnd.ms-excel", "reporte_" + id + ".xls");
        }

        public class ReporteSimple
        {
            public byte[] Exportar(string BusquedaAnterior, List<VendedorDto> vendedores)
            {
                //logger.InfoFormat("Se exporto la busqueda de: {0}", BusquedaAnterior);

                // codigo del Reporte
                int numReg = vendedores.Count();
                //usar esto cuanto tienes un template
                //var nombreArchivo = NombreArchivo(); //@"d:\plantillas\balance.xlt";
                //var fs = new FileStream(nombreArchivo, FileMode.CreateNew);
                //var libro = new HSSFWorkbook(fs, true);




                var libro = new HSSFWorkbook();

                var hoja = libro.CreateSheet("Ejemplo Reporte");

                var fila = hoja.CreateRow(0);
                var celda = fila.CreateCell(0);
                celda.SetCellValue("Lista de Vendedores");
                //celda.CellStyle.WrapText = true;

                fila = hoja.CreateRow(1);
                celda = fila.CreateCell(0);
                celda.SetCellValue("Código");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(1);
                celda.SetCellValue("Tipo Vendedor");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(2);
                celda.SetCellValue("Nombes");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(3);
                celda.SetCellValue("Documento");
                //celda.CellStyle.WrapText = true;

                celda = fila.CreateCell(4);
                celda.SetCellValue("Estado");
                //celda.CellStyle.WrapText = true;

                int count = 1;
                foreach (var vendedor in vendedores.ToList())
                {
                    count += 1;
                    fila = hoja.CreateRow(count);

                    celda = fila.CreateCell(0);
                    celda.SetCellValue(vendedor.VendedorId);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(1);
                    celda.SetCellValue(vendedor.TipoVendedor);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(2);
                    celda.SetCellValue(vendedor.Nombres);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(3);
                    celda.SetCellValue(vendedor.NumeroDocumento);
                    //celda.CellStyle.WrapText = true;

                    celda = fila.CreateCell(4);
                    celda.SetCellValue(vendedor.Estado);
                    //celda.CellStyle.WrapText = true;
                }

                //var celda2 = fila1.CreateCell(1);
                //celda2.SetCellValue("10/01/2016");

                var mstream = new MemoryStream();
                libro.Write(mstream);
                return mstream.ToArray();

                //return View();

            }
            private string NombreArchivo()
            {
                var ruta = Path.GetTempFileName();
                System.IO.File.Delete(ruta);
                return Path.ChangeExtension(ruta, "xls");
            }
        }

    }
}