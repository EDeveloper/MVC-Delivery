﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Web.Models;
using Ventas.Servicios.Impl;
using Ventas.Servicios.Dtos;
using AutoMapper;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class BuscarDespachosController : Controller
    {
        private ServicioVentaBusqueda _servicioBusqueda;
        // GET: Buscar
        public BuscarDespachosController()
        {
            _servicioBusqueda = new ServicioVentaBusqueda();
        }
        public ActionResult Index()
        {
            return View(new BuscaDespachosViewModel());
        }

        public PartialViewResult BuscarDespachos(string busqueda)
        {
            //if (!ModelState.IsValid)
            //{
            //    return View(new BuscaDespachosViewModel());
            //}


            IEnumerable<DespachoDto> ventas;
            if (string.IsNullOrEmpty(busqueda))
            {
                TempData["Success"] = "Debe ingresar la fecha de despacho";
                ventas = new List<DespachoDto>();
            }
            else
            {
                ventas = _servicioBusqueda.BuscarDespachos(busqueda);

                if (ventas.Count() == 0)
                {
                    TempData["Success"] = "No tiene despachos programados";
                }
            }
            //ViewBag.Ventas = ventas;
            return PartialView("_DespachosEncontradas", Mapper.Map<IEnumerable<DespachoDto>, IList<ListaDespachosViewModel>>(ventas));

        }
    }
}