﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;
using Ventas.Servicios.Dtos;

namespace Ventas.Web.Controllers
{
    public class EntregasController : Controller
    {
        private ServicioRutas servicioRutas;
        public EntregasController()
        {
            servicioRutas = new ServicioRutas();
        }
        // GET: Rutas
        public ActionResult Inicio()
        {
            return View(InicializaModelo());
        }


        public PartialViewResult ObtieneRutas(EntregasRutasViewModel model)
        {
            var rutas = servicioRutas.RutaEntregas(model.VehiculoSeleccionado, model.FechaVenta);
            if (rutas.Count <= 0) @TempData["Success"] = "No se encontraron entregas pendientes";
            var modelo = new EntregasViewModel(servicioRutas.Vehiculos())
            {
                Rutas = rutas,
                FechaRuta = model.FechaVenta,
                VehiculoSeleccionado = model.VehiculoSeleccionado
            };
            return PartialView("_GuardaRutas", modelo);
        }

        [HttpPost]
        public ActionResult Guardar(EntregasGuardarViewModel entrega)
        {
            try
            {
                if (entrega.Rutas.Count() > 0)
                {
                    servicioRutas.GrabaRutaEntrega(entrega.Rutas, entrega.VehiculoSeleccionado, entrega.FechaRuta);
                    TempData["Success"] = "Se genero la RUTA correctamente";
                }
                //return View(InicializaModelo());
                var modelo = new EntregasViewModel(servicioRutas.Vehiculos())
                {
                    Rutas = entrega.Rutas,
                    VehiculoSeleccionado = entrega.VehiculoSeleccionado
                };

                //return View("Inicio", modelo);
                return RedirectToAction("Inicio", modelo);

            }
            catch (Exception)
            {
                return View(InicializaModelo());
                //return RedirectToAction("Inicio", modelo);
                //var modelo = new EntregasViewModel(servicioRutas.Vehiculos())
                //{
                //    Rutas = entrega.Rutas,
                //    VehiculoSeleccionado = entrega.VehiculoSeleccionado
                //};
                //TempData["Success"] = "Error al la generar RUTA";
                //return View("Inicio", modelo);
            }
        }


        private EntregasViewModel InicializaModelo()
        {
            var modelo = new EntregasViewModel(servicioRutas.Vehiculos()) { Rutas = new List<EntregasDto>() };
            return modelo;
        }

    }
}