﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;
using System.Net;
using System.Xml.Linq;
using System.Globalization;

using System.Device.Location;

namespace Ventas.Web.Controllers
{
    [Authorize]

    public class ClientesController : Controller
    {
        private ServicioClientes servicioClientes;
        public ClientesController()
        {
            servicioClientes = new ServicioClientes();
        }
        // GET: Clientes




        public ActionResult Ver(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);

            return View(
                Mapper.Map<ClienteDto, ClienteDetalleViewModel>(cliente)
                );
        }

        public ActionResult Modificar(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);
            //TempData["Success"]
            //ViewBag.Success = "Se Geolocalizo correctamente la dirección";
            return View(
                Mapper.Map<ClienteDto, ClienteDetalleViewModel>(cliente)
                );
        }

        //public ActionResult Despachar(string fecha)
        //{
        //    VentaDto despacho = servicioClientes.TraerDespacho(fecha);

        //    var model = new DespachoViewModel()
        //    {
        //        ClienteOrigen = despacho.ClienteNombre,
        //        ClienteDestino = despacho.ClienteNombre
        //    };
        //    return View("Despachar", model);

        //}

        public PartialViewResult Geolocalizar(int id)
        {
            DireccionDto direccion = servicioClientes.TraerDireccion(id);

            var model = new GeolocalizacionViewModel()
            {
                Descripcion = direccion.Descripcion.Trim(),
                DireccionId = direccion.DireccionId,
                ClienteId = direccion.ClienteId,
                Departamento = direccion.Departamento,
                Provincia = direccion.Provincia,
                Distrito = direccion.Distrito,
                GeoFlag = direccion.GeoFlag,
            };

            //var address = direccion.Descripcion;
            //var requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false",
            //        Uri.EscapeDataString(address));

            //var request = WebRequest.Create(requestUri);
            //var response = request.GetResponse();
            //var xdoc = XDocument.Load(response.GetResponseStream());

            //var result = xdoc.Element("GeocodeResponse").Element("result");
            //if (result != null)
            //{
            //    var locationElement = result.Element("geometry").Element("location");
            //    model.Latitud = locationElement.Element("lat").Value;
            //    model.Longitud = locationElement.Element("lng").Value;
            //    model.GeoFlag = "S";
            //}
            return PartialView("_Geolocalizar", model);
        }

        public static double CalculateDistance(double latA, double longA, double latB, double longB)
        {
            var locA = new GeoCoordinate(latA, longA);
            var locB = new GeoCoordinate(latB, longB);
            double distance = locA.GetDistanceTo(locB); // m
            distance = distance * 1.609344; //Kilometers -> default
            return distance;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Geolocalizar(GeolocalizacionViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    servicioClientes.Geolocalizar(new GeolocalizacionDto()
                    {
                        DireccionId = model.DireccionId,
                        ClienteId = model.ClienteId,
                        Direccion = model.Descripcion,
                        Latitud = model.Latitud,
                        Longitud = model.Longitud,
                        Departamento = model.Departamento,
                        Provincia = model.Provincia,
                        Distrito = model.Distrito,
                        GeoFlag = model.GeoFlag
                    });
                }
                //ViewBag.Success = "Se Geolocalizo correctamente la dirección";
                TempData["Success"] = "Se Geolocalizo correctamente la dirección";
                return RedirectToAction("Modificar", "Clientes", new { id = model.ClienteId });
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("Aviso", "Hubo un Error: " + ex);
                return RedirectToAction("Modificar", "Clientes", new { id = model.ClienteId });
                //throw;
            }

        }

        public PartialViewResult Direccion(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);
            var model = new InscribirseViewModel()
            {
                Titulo = cliente.ClienteNombre,
                EventoId = cliente.ClienteId
            };
            return PartialView("_Geolocation", model);
        }

        public PartialViewResult Create()
        {
            ClienteDetalleViewModel model = new ClienteDetalleViewModel();
            return PartialView("_Create", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ClienteDetalleViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ClienteDto cliente = new ClienteDto();
                    cliente.ClienteId = model.ClienteId;
                    cliente.TipoDocumento = model.TipoDocumento;
                    cliente.NumeroDocumento = model.NumeroDocumento;
                    cliente.ClienteNombre = model.ClienteNombre;
                    cliente.Direccion = model.Direccion;
                    cliente.Telefono = model.Telefono;
                    cliente.CorreoElectronico = model.CorreoElectronico;
                    cliente.Estado = model.Estado;
                    //cliente.Latitud = model.Latitud;
                    //cliente.Longitud = model.Longitud;
                    servicioClientes.Create(cliente);
                    //model.ClienteId = cliente.ClienteId;
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    return RedirectToAction("Index", "BuscarClientes", new { id = model.ClienteId });
                    //return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
        }

        public PartialViewResult Details(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);
            var model = new ClienteDetalleViewModel()
            {
                ClienteId = cliente.ClienteId,
                TipoDocumento = cliente.TipoDocumento,
                NumeroDocumento = cliente.NumeroDocumento,
                ClienteNombre = cliente.ClienteNombre,
                Direccion = cliente.Direccion,
                Telefono = cliente.Telefono,
                CorreoElectronico = cliente.CorreoElectronico,
                Estado = cliente.Estado,
                //Latitud = cliente.Latitud,
                //Longitud = cliente.Longitud

            };
            return PartialView("_Details", model);
        }

        public PartialViewResult Edit(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);
            var model = new ClienteDetalleViewModel()
            {
                ClienteId = cliente.ClienteId,
                ClienteNombre = cliente.ClienteNombre,
                Direccion = cliente.Direccion,
                CorreoElectronico = cliente.CorreoElectronico,
                NumeroDocumento = cliente.NumeroDocumento,
                TipoDocumento = cliente.TipoDocumento,
                Telefono = cliente.Telefono,
                //Latitud = cliente.Latitud,
                //Longitud = cliente.Longitud,
                Estado = cliente.Estado
            };
            return PartialView("_Edit", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ClienteDetalleViewModel model, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ClienteDto cliente = servicioClientes.Traer(id);
                    cliente.ClienteId = model.ClienteId;
                    cliente.TipoDocumento = model.TipoDocumento;
                    cliente.NumeroDocumento = model.NumeroDocumento;
                    cliente.ClienteNombre = model.ClienteNombre;
                    cliente.Direccion = model.Direccion;
                    cliente.Telefono = model.Telefono;
                    cliente.CorreoElectronico = model.CorreoElectronico;
                    cliente.Estado = model.Estado;
                    //cliente.Latitud = model.Latitud;
                    //cliente.Longitud = model.Longitud;
                    servicioClientes.Update(cliente);
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
            return RedirectToAction("Index", "BuscarClientes", new { id = model.ClienteId });

        }

        public ViewResult Marker(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);
            var model = new ClienteDetalleViewModel()
            {
                ClienteId = cliente.ClienteId,
                ClienteNombre = cliente.ClienteNombre,
                Direccion = cliente.Direccion,
                CorreoElectronico = cliente.CorreoElectronico,
                NumeroDocumento = cliente.NumeroDocumento,
                TipoDocumento = cliente.TipoDocumento,
                Telefono = cliente.Telefono,
                //Latitud = cliente.Latitud,
                //Longitud = cliente.Longitud,
                Estado = cliente.Estado
            };
            return View("_Marker", model);
        }
        //public PartialViewResult Marker(int id)
        //{
        //    ClienteDto cliente = servicioClientes.Traer(id);
        //    var model = new ClienteDetalleViewModel()
        //    {
        //        ClienteId = cliente.ClienteId,
        //        ClienteNombre = cliente.ClienteNombre,
        //        Direccion = cliente.Direccion,
        //        CorreoElectronico = cliente.CorreoElectronico,
        //        NumeroDocumento = cliente.NumeroDocumento,
        //        TipoDocumento = cliente.TipoDocumento,
        //        Telefono = cliente.Telefono,
        //        Latitud = cliente.Latitud,
        //        Longitud = cliente.Longitud,
        //        Estado = cliente.Estado
        //    };
        //    return PartialView("_Marker", model);
        //}

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Marker(ClienteDetalleViewModel model, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ClienteDto cliente = servicioClientes.Traer(id);
                    cliente.ClienteId = model.ClienteId;
                    cliente.TipoDocumento = model.TipoDocumento;
                    cliente.NumeroDocumento = model.NumeroDocumento;
                    cliente.ClienteNombre = model.ClienteNombre;
                    cliente.Direccion = model.Direccion;
                    cliente.Telefono = model.Telefono;
                    cliente.CorreoElectronico = model.CorreoElectronico;
                    cliente.Estado = model.Estado;
                    //cliente.Latitud = model.Latitud;
                    //cliente.Longitud = model.Longitud;
                    servicioClientes.Update(cliente);
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
            return RedirectToAction("Index", "BuscarClientes", new { id = model.ClienteId });

        }

        public PartialViewResult Delete(int id)
        {
            ClienteDto cliente = servicioClientes.Traer(id);
            var model = new ClienteDetalleViewModel()
            {
                ClienteId = cliente.ClienteId,
                ClienteNombre = cliente.ClienteNombre,
                Direccion = cliente.Direccion,
                CorreoElectronico = cliente.CorreoElectronico,
                NumeroDocumento = cliente.NumeroDocumento,
                TipoDocumento = cliente.TipoDocumento,
                Telefono = cliente.Telefono,
                //Latitud = cliente.Latitud,
                //Longitud = cliente.Longitud,
                Estado = cliente.Estado
            };
            return PartialView("_Delete", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(ClienteDetalleViewModel model, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ClienteDto cliente = servicioClientes.Traer(id);
                    servicioClientes.Delete(cliente);
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "BuscarClientes", new { id = 1 });
        }

        public ActionResult DeleteSelected(string[] ids)
        {
            //Delete Selected 
            int[] id = null;
            if (ids != null)
            {
                id = new int[ids.Length];
                int j = 0;
                foreach (string i in ids)
                {
                    int.TryParse(i, out id[j++]);
                }
            }

            if (id != null && id.Length > 0)
            {
                //List<CustomerInfo> allSelected = new List<CustomerInfo>();
                //using (MyDatabaseEntities dc = new MyDatabaseEntities())
                //{
                //allSelected = dc.CustomerInfoes.Where(a => id.Contains(a.CustomerID)).ToList();
                //foreach (var i in allSelected)
                //{
                //    dc.CustomerInfoes.Remove(i);
                //}
                //dc.SaveChanges();
                //}
            }
            return RedirectToAction("List");
        }
    }
}