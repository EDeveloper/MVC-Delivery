﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {

        private ServicioClienteBusqueda _servicioBusqueda;
        public HomeController()
        {
            _servicioBusqueda = new ServicioClienteBusqueda();
        }
        public ActionResult Index()
        {
            var saludo = "";

            if (DateTime.Now.Hour < 12)
            {
                saludo = "Buenos días ";
            }
            else if (DateTime.Now.Hour < 17)
            {
                saludo = "Buenos tardes ";
            }
            else
            {
                saludo = "Buenos Noches  ";
            }

            var nombreUsuario = Request.RequestContext.HttpContext.User.Identity.Name;
            @TempData["Success"] = saludo + " " + nombreUsuario + " Bievenido al sistema";
            return View();
        }


        public ActionResult loaddata()
        {

            IEnumerable<ClienteDto> clientes;
            clientes = _servicioBusqueda.Buscar("", "", "");
            return Json(new { data = clientes }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

    }
}