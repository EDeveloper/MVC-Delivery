﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Web.Models;
using Ventas.Servicios.Impl;
using Ventas.Servicios.Dtos;
using AutoMapper;
using System.Threading.Tasks;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class BuscarVentasController : Controller
    {
        private ServicioVentaBusqueda _servicioBusqueda;
        private ServicioChoferes _servicioChoferes;
        private ServicioVehiculos _servicioVehiculos;

        // GET: Buscar
        public BuscarVentasController()
        {
            _servicioBusqueda = new ServicioVentaBusqueda();
            _servicioChoferes = new ServicioChoferes();
            _servicioVehiculos = new ServicioVehiculos();
        }
        public ActionResult Index()
        {
            return View(new BuscaVentasViewModel());
            //return View(new SeleccionarVentasViewModel());
        }

        public PartialViewResult BuscarVentas(string busqueda)
        {
            IEnumerable<VentaDto> ventas ;
            if (string.IsNullOrEmpty(busqueda))
                ventas = new List<VentaDto>();
            else
                ventas = _servicioBusqueda.Buscar(busqueda);

             //chofer = _servicioBusqueda.Buscar(busqueda);

            //ViewBag.Chofer = new SelectList(db. .CompaniaMast.Where(a => a.Estado == "A"), "CompaniaCodigo", "DescripcionCorta", companiaCodigo);
            //ViewBag.Vehiculo = new SelectList(db.CuentaBancaria.Where(a => a.Estado == "A"), "CuentaBancaria1", "Descripcion", cuentaBancaria);

            var chofer = _servicioChoferes.BuscarChoferes("");
            var vehiculos = _servicioVehiculos.BuscarVehiculos("");


            //ViewBag.Chofer = new SelectList(chofer.ToList());
            //ViewBag.Vehiculo = new SelectList(vehiculos.Select(u => u.PlacaVehiculo).Distinct());

            ViewBag.Chofer = new SelectList(chofer.Select(u => u.Nombres).Distinct());

            //ViewBag.Vehiculo = new SelectList(vehiculos.Select(u => u.PlacaVehiculo).Distinct());


            ViewBag.Vehiculo = new SelectList(vehiculos.Select(u => u.PlacaVehiculo).Distinct());



            return PartialView("_VentasEncontradas", Mapper.Map<IEnumerable<VentaDto>, IList<ListaVentasViewModel>>(ventas));

        }

        //public PartialViewResult SelectVentas(string busqueda)

        public ActionResult SelectVentas(String busqueda)
        {

            IEnumerable<VentaDto> ventas;
            if (string.IsNullOrEmpty(busqueda))
                ventas = new List<VentaDto>();
            else
                ventas = _servicioBusqueda.Buscar(busqueda);

            foreach (var item in ventas)
            {
                var model = new SeleccionarVentasViewModel()
                    {Selected = true
                    };
            }
            return View(ventas);
            //return PartialView("_VentasEncontradas", Mapper.Map<IEnumerable<VentaDto>, IList<ListaVentasViewModel>>(ventas));

        }
    }
}