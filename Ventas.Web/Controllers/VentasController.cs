﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class VentasController : Controller
    {
        private ServicioVentas servicioVentas;
        public VentasController()
        {
            servicioVentas = new ServicioVentas();
        }
        // GET: Eventos
        public ActionResult Ver(int id)
        {
            VentaDto venta = servicioVentas.Traer(id);
            
            return View(
                Mapper.Map<VentaDto, VentaDetalleViewModel>(venta)
                );
        }

        public ActionResult VerDespacho(int id)
        {
            DespachoDto venta = servicioVentas.TraerDespacho(id);

            return View(
                Mapper.Map<DespachoDto, ListaDespachosViewModel>(venta)
                );
        }

        public ActionResult VentasSelected(string chofer, string vehiculo, string[] ids)
        {
            //Delete Selected 
            int[] id = null;
            if (ids != null)
            {
                id = new int[ids.Length];
                int j = 0;
                foreach (string i in ids)
                {
                    int.TryParse(i, out id[j++]);
                }
            }
           
            if (id != null && id.Length > 0)
            {
                //List<CustomerInfo> allSelected = new List<CustomerInfo>();
                //using (MyDatabaseEntities dc = new MyDatabaseEntities())
                //{
                //    allSelected = dc.CustomerInfoes.Where(a => id.Contains(a.CustomerID)).ToList();
                 //VentaDto venta = servicioVentas.Traer(id);
                    //VentaDto venta  = new VentaDto();
                    foreach (var i in id.ToList() )
                    {
                        UpdateDespacho(i, chofer, vehiculo);
                        //venta = servicioVentas.Traer(i);
                        //venta.EstadoDespacho = "Despachado";
                    }
                    //dc.SaveChanges();
                }
            
            return RedirectToAction("Index","BuscarVentas");
        }

        private string UpdateDespacho(int id, string chofer, string vehiculo)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    VentaDto venta = servicioVentas.Traer(id);
                    venta.EstadoDespacho = "DESPACHADO";
                    venta.FechaDespacho = DateTime.Now;
                    //venta.Chofer = chofer;
                    //venta.Vehiculo = vehiculo;

                    //venta.ClienteNombre = model.ClienteNombre;
                    //venta.Direccion = model.Direccion;
                    //venta.Telefono = model.Telefono;
                    //venta.CorreoElectronico = model.CorreoElectronico;
                    //venta.Estado = model.Estado;
                    //venta.Latitud = model.Latitud;
                    //venta.Longitud = model.Longitud;
                    servicioVentas.Update(venta);
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    //return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return "";
            //return RedirectToAction("Ver", "Clientes", new { id = model.ClienteId });
            //return RedirectToAction("Index", "BuscarClientes", new { id = model.ClienteId });

        }

        //public PartialViewResult Create()
        //{
        //    VentaDetalleViewModel model = new VentaDetalleViewModel();
        //    return PartialView("_Create", model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(VentaDetalleViewModel model)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            VentaDto venta = new VentaDto();
        //            venta.VentaId = model.VentaId;
        //            venta.TipoDocumento = model.TipoDocumento;
        //            venta.NumeroDocumento = model.NumeroDocumento;
        //            venta.ClienteNombre = model.ClienteNombre;
        //            //venta.Direccion = model.Direccion;
        //            //venta.Telefono = model.Telefono;
        //            //venta.CorreoElectronico = model.CorreoElectronico;
        //            //venta.EstadoVenta = model.Estado;
        //            //venta.Latitud = model.Latitud;
        //            //venta.Longitud = model.Longitud;
        //            servicioVentas.Create(venta);
        //            //model.ClienteId = venta.ClienteId;
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Hubo un Error");
        //            return RedirectToAction("Ver", "Ventas", new { id = model.VentaId });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return RedirectToAction("Ver", "Ventas", new { id = model.VentaId });
        //}

        //public PartialViewResult Details(int id)
        //{
        //    VentaDto cliente = servicioVentas.Traer(id);
        //    var model = new VentaDetalleViewModel()
        //    {
        //        //ClienteId = cliente.ClienteId,
        //        //TipoDocumento = cliente.TipoDocumento,
        //        //NumeroDocumento = cliente.NumeroDocumento,
        //        //ClienteNombre = cliente.ClienteNombre,
        //        //Direccion = cliente.Direccion,
        //        //Telefono = cliente.Telefono,
        //        //CorreoElectronico = cliente.CorreoElectronico,
        //        //Estado = cliente.Estado,
        //        //Latitud = cliente.Latitud,
        //        //Longitud = cliente.Longitud

        //    };
        //    return PartialView("_Details", model);
        //}

        //public PartialViewResult Edit(int id)
        //{
        //    VentaDto cliente = servicioVentas.Traer(id);
        //    var model = new VentaDetalleViewModel()
        //    {
        //        //ClienteId = cliente.ClienteId,
        //        //ClienteNombre = cliente.ClienteNombre,
        //        //Direccion = cliente.Direccion,
        //        //CorreoElectronico = cliente.CorreoElectronico,
        //        //NumeroDocumento = cliente.NumeroDocumento,
        //        //TipoDocumento = cliente.TipoDocumento,
        //        //Telefono = cliente.Telefono,
        //        //Latitud = cliente.Latitud,
        //        //Longitud = cliente.Longitud,
        //        //Estado = cliente.Estado
        //    };
        //    return PartialView("_Edit", model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(VentaDetalleViewModel model, int id)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            VentaDto cliente = servicioVentas.Traer(id);
        //            //cliente.ClienteId = model.ClienteId;
        //            //cliente.TipoDocumento = model.TipoDocumento;
        //            //cliente.NumeroDocumento = model.NumeroDocumento;
        //            //cliente.ClienteNombre = model.ClienteNombre;
        //            //cliente.Direccion = model.Direccion;
        //            //cliente.Telefono = model.Telefono;
        //            //cliente.CorreoElectronico = model.CorreoElectronico;
        //            //cliente.Estado = model.Estado;
        //            //cliente.Latitud = model.Latitud;
        //            //cliente.Longitud = model.Longitud;
        //            servicioVentas.Update(cliente);
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Hubo un Error");
        //            return RedirectToAction("Ver", "Ventas", new { id = model.VentaId });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return RedirectToAction("Ver", "Ventas", new { id = model.VentaId });
        //}

        //public PartialViewResult Delete(int id)
        //{
        //    VentaDto cliente = servicioVentas.Traer(id);
        //    var model = new VentaDetalleViewModel()
        //    {
        //        //ClienteId = cliente.ClienteId,
        //        //ClienteNombre = cliente.ClienteNombre,
        //        //Direccion = cliente.Direccion,
        //        //CorreoElectronico = cliente.CorreoElectronico,
        //        //NumeroDocumento = cliente.NumeroDocumento,
        //        //TipoDocumento = cliente.TipoDocumento,
        //        //Telefono = cliente.Telefono,
        //        //Latitud = cliente.Latitud,
        //        //Longitud = cliente.Longitud,
        //        //Estado = cliente.Estado
        //    };
        //    return PartialView("_Delete", model);
        //}

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Delete(VentaDetalleViewModel model, int id)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            VentaDto cliente = servicioVentas.Traer(id);
        //            servicioVentas.Delete(cliente);
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "Hubo un Error");
        //            return RedirectToAction("Ver", "Ventas", new { id = model.VentaId });
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return RedirectToAction("Ver", "Ventas", new { id = 1 });
        //}
    }
}