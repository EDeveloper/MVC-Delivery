﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ventas.Web.Models;
using Ventas.Servicios.Impl;
using Ventas.Servicios.Dtos;
using AutoMapper;
using System.Net;
using System.Xml.Linq;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class BuscarClientesController : Controller
    {
        private ServicioClienteBusqueda _servicioBusqueda;
        // GET: Buscar
        public BuscarClientesController()
        {
            _servicioBusqueda = new ServicioClienteBusqueda();
        }
        public ActionResult Index()
        {
            return View(new BuscaClientesViewModel());
        }

        public ActionResult IndexGeo()
        {
            return View(new ClientesGeoSearchModel());
        }

        public PartialViewResult BuscarClientes(string codigo, string documento, string busqueda)
        {
            IEnumerable<ClienteDto> clientes;

            if (string.IsNullOrEmpty(documento) && string.IsNullOrEmpty(busqueda) && string.IsNullOrEmpty(busqueda))
                clientes = new List<ClienteDto>();
            else
                clientes = _servicioBusqueda.Buscar(codigo, documento, busqueda);

            return PartialView("_ClientesEncontrados", Mapper.Map<IEnumerable<ClienteDto>, IList<ListaClientesViewModel>>(clientes));
        }

        public PartialViewResult BuscarClientesGeo(string codigo, string documento, string busqueda, string tipo)
        {
            IEnumerable<GeolocalizacionDto> clientes;
            //clientes = new List<GeolocalizacionDto>();

            //if (string.IsNullOrEmpty(busquedaCliente) && string.IsNullOrEmpty(busquedaDocumento))
            //    clientes = new List<GeolocalizacionDto>();
            //else
            clientes = _servicioBusqueda.BuscarGeo(codigo, documento, busqueda, tipo);

            if (clientes.Count() == 0)
            {
                TempData["Success"] = "No se han encontrado resultados con los criterios de búsqueda ingresados";
            }

            return PartialView("_ClientesGeoEncontrados",
                    Mapper.Map<IEnumerable<GeolocalizacionDto>, IList<ClientesGeoListModel>>(clientes));
        }


        public ActionResult ActualizarDireccionGeo(string Location)
        {
            var direcciones = _servicioBusqueda.TraerDirecciones(Location, "N");
            string message1="";
            string message2="";
            string message3="";
            var contadorOK = 0;
            var contadorError = 0;
            var contadorLimit = 0;
            //AIzaSyBsUQzQF5gMVia17P7xWZS5ngIiCuk4cLg 
            foreach (var direccion in direcciones.ToList())
            {
                var address = direccion.Descripcion.Trim();
                var id = direccion.DireccionId;

                if (direccion.GeoFlag == "N")
                {

                    try
                    {
                        var requestUri = string.Format("http://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false", Uri.EscapeDataString(address));
                        //var requestUri = string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&sensor=false,+CA&key=AIzaSyBsUQzQF5gMVia17P7xWZS5ngIiCuk4cLg", Uri.EscapeDataString(address));

                        var request = WebRequest.Create(requestUri);
                        var response = request.GetResponse();
                        var xdoc = XDocument.Load(response.GetResponseStream());
                        string status = xdoc.Element("GeocodeResponse").Element("status").Value;
                        var result = xdoc.Element("GeocodeResponse").Element("result");
                        if (status == "ZERO_RESULTS")
                        {
                            contadorError++;
                            direccion.GeoFlag = "E";

                        }
                        else if (status == "OVER_QUERY_LIMIT")
                        {
                            contadorLimit++;
                            direccion.GeoFlag = "N";
                        }
                        if (result != null)
                        {
                            contadorOK++;
                            var locationElement = result.Element("geometry").Element("location");
                            direccion.Latitud = locationElement.Element("lat").Value;
                            direccion.Longitud = locationElement.Element("lng").Value;
                            direccion.GeoFlag = "S";

                        }
                        DireccionDto direccionUpdate = _servicioBusqueda.TraerDireccion(id);
                        direccionUpdate.Latitud = direccion.Latitud;
                        direccionUpdate.Longitud = direccion.Longitud;
                        direccionUpdate.GeoFlag = direccion.GeoFlag;
                        _servicioBusqueda.UpdateDireccion(direccionUpdate);
                    }
                    catch (Exception ex)
                    {
                        return View("Error", ex);
                    }


                }
            }
            //var message = "";
            message1 = string.Format("Se excedio el limite de consultas API Google : \"{0}\".", contadorLimit);
            message2 = string.Format("Direcciones Geolocalizadas : \"{0}\".", contadorOK);
            message3 = string.Format("Direcciones no encontradas : \"{0}\".", contadorError);

            var message = message1 + " " + message2 + " " + message3;
            TempData["Success"] = message1 + " " + message2 + " " + message3;
            return Json(message, JsonRequestBehavior.AllowGet);
            //var modelo = new ClientesGeoSearchModel()
            //{
            //    Tipo = Location,
            //};

            ////return View("Inicio", modelo);
            //return RedirectToAction("IndexGeo", modelo);

            //return View(new ClientesGeoSearchModel());

            //var result2 = Mapper.Map<IEnumerable<DireccionDto>, IList<ListaLocationsViewModel>>(direcciones);
            //return Json(message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult IndexMap()
        {
            var direccion = _servicioBusqueda.TraerDirecciones("", "S");
            return View(Mapper.Map<IEnumerable<DireccionDto>, IList<ListaLocationsViewModel>>(direccion));
        }

        public ActionResult BuscarClientesMapa(string Location)
        {
            var direccion = _servicioBusqueda.TraerDirecciones(Location, "S");
            var result = Mapper.Map<IEnumerable<DireccionDto>, IList<ListaLocationsViewModel>>(direccion);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}