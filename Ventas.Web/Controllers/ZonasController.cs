﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPOI.HSSF.UserModel;
using System.IO;
using System.Web.Mvc;
using AutoMapper;
using Ventas.Servicios.Dtos;
using Ventas.Servicios.Impl;
using Ventas.Web.Models;


//using System.Collections.Generic;
//using System.Linq;
//using System.Web;


//using System.Net;
//using System.Xml.Linq;
//using System.IO;
//using NPOI.HSSF.UserModel;

namespace Ventas.Web.Controllers
{
    [Authorize]
    public class ZonasController : Controller
    {
        private ServicioZonas _servicioZonas;
        public ZonasController()
        {
            _servicioZonas = new ServicioZonas();
        }

        public ActionResult Index()
        {
            return View(new ZonasSearchModel());
        }


        public PartialViewResult BuscarZonas(string busqueda)
        {
            IEnumerable<ZonaDto> zonas;
            zonas = _servicioZonas.BuscarZonas(busqueda);
            return PartialView("_ZonasEncontrados", Mapper.Map<IEnumerable<ZonaDto>, IList<ZonasListModel>>(zonas));

        }


        public ActionResult Edit(int id)
        {
            ZonaDto zona = _servicioZonas.Traer(id);
            return View(
                Mapper.Map<ZonaDto, ZonasEditModel>(zona)
                );
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ZonasEditModel model, int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ZonaDto zona = _servicioZonas.Traer(id);
                    zona.ZonaId = model.ZonaId;
                    zona.ZonaCodigo = model.ZonaCodigo;
                    zona.Descripcion = model.Descripcion;
                    zona.Departamento = model.Departamento;
                    zona.Provincia = model.Provincia;
                    zona.Distrito = model.Distrito;
                    //zona.Coordenadas = model.Coordenadas;
                    zona.Estado = model.Estado;
                    zona.UltimoUsuario = model.UltimoUsuario;
                    zona.UltimaFechaModif = model.UltimaFechaModif;
                    _servicioZonas.Update(zona);
                }
                else
                {
                    ModelState.AddModelError("", "Hubo un Error");
                    return RedirectToAction("Ver", "Zonas", new { id = model.ZonaId });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "BuscarZonas", new { id = model.ZonaId });

        }

        //public PartialViewResult Edit(int id)
        //{
        //    ZonaDto zona = _servicioZonas.Traer(id);
        //    var model = new ZonasEditModel()
        //    {
        //        ZonaId = zona.ZonaId,
        //        ZonaCodigo = zona.ZonaCodigo,
        //        Descripcion = zona.Descripcion,
        //        Departamento = zona.Departamento,
        //        Provincia = zona.Provincia,
        //        Distrito = zona.Distrito,
        //        Coordenadas = zona.Coordenadas,
        //        Estado = zona.Estado,
        //        UltimoUsuario = zona.UltimoUsuario,
        //        UltimaFechaModif = zona.UltimaFechaModif
        //    };
        //    return PartialView("_Edit", model);
        //}

    }
}