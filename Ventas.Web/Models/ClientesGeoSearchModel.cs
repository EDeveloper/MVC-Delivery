﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class ClientesGeoSearchModel
    {
        public ClientesGeoSearchModel()
        {
            Clientes = new List<ClientesGeoListModel>();
        }
        public string  Codigo { get; set; }
        public string Documento { get; set; }
        public string Busqueda { get; set; }
        public string Tipo { get; set; }
        public IEnumerable<ClientesGeoListModel> Clientes { get; set; }

    }
}