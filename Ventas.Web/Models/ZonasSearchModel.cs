﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class ZonasSearchModel
    {
        public ZonasSearchModel()
        {
            Zonas = new List<ZonasListModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<ZonasListModel> Zonas { get; set; }
    }
}