﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ventas.Web.Models
{
    public class PapeletasListModel
    {
        public int PapeletaId { get; set; }
        public string NumeroPapeleta { get; set; }
        public decimal MontoPapeleta { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Direccion { get; set; }
        public string Distrito { get; set; }
        public string CodigoDistrito { get; set; }
        public string Brevete { get; set; }
        public string CategoriaBrevete { get; set; }
        public string PlacaVehiculo { get; set; }
        public string ChoferCodigo { get; set; }
        public DateTime FechaPapeleta { get; set; }
        public string TipoInfraccion { get; set; }
        public string Estado { get; set; }
    }
}
