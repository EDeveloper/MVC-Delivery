﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Ventas.Web.Models
{
    public class ListaClientesViewModel
    {
        public int ClienteId { get; set; }
        
        public string ClienteNombre { get; set; }
        [DisplayName("Tipo")]
        public string TipoDocumento { get; set; }
                [DisplayName("Número")]
        public string NumeroDocumento { get; set; }
                [DisplayName("E-Mail")]
        public string CorreoElectronico { get; set; }
                [DisplayName("Teléfono")]
        public string Telefono { get; set; }
                [DisplayName("Dirección")]
        public string Direccion { get; set; }
                [DisplayName("Estado")]
        public string Estado { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string TipoCliente { get; set; }
        public int CodigoCliente { get; set; }
    }
}
