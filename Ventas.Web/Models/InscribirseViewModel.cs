﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class InscribirseViewModel
    {
        [Display(Description = "Dirección")]
        public string  Titulo { get; set; }
        [Required]
        [Display(Description="Asistente")]
        [MinLength(2)]
        public string NombreAsistente { get; set; }

        [Required]
        [Display(Description = "#Entradas")]
        public int NumeroEntradas { get; set; }

        //public string Direccion { get; set; }
        //public string Latitud { get; set; }
        //public string Longitud { get; set; }
        //public string Departamento { get; set; }
        //public string Provincia { get; set; }
        //public string Distrito { get; set; }
        public int EventoId { get; set; }

    }
}