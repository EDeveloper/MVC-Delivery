﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace Ventas.Web.Models
{
    public class ClienteDetalleViewModel
    {
        public int ClienteId { get; set; }
        public int ClienteCodigo { get; set; }
        public string TipoCliente { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string ClienteNombre { get; set; }
        public string Direccion { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Estado { get; set; }
        public Decimal RatioVenta { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }

        public IEnumerable<ClienteDireccion> Direcciones { get; set; }
    }
    public class ClienteDireccion
    {
        public int DireccionId { get; set; }
        public int ClienteId { get; set; } //EF
        public int ZonaId { get; set; } //EF
        public int ClienteCodigo { get; set; }
        public int DireccionCodigo { get; set; }
        public string Descripcion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        public string GeoFlag { get; set; }


    }
}
