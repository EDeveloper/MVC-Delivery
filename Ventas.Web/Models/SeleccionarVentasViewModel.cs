﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ventas.Web.Models
{
    public class SeleccionarVentasViewModel
    {
        public int VentaId { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaDespacho { get; set; }
        public string ClienteNombre { get; set; }
        public string ClienteDireccion { get; set; }
        public decimal MontoAfecto { get; set; }
        public decimal MontoNoAfecto { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal MontoTotal { get; set; }
        public string EstadoVenta { get; set; }
        public string EstadoDespacho { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public bool Selected { get; set; }
        public SeleccionarVentasViewModel() { }

    }
}
