﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class ChoferesSearchModel
    {
        public ChoferesSearchModel()
        {
            Choferes = new List<ChoferesListModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<ChoferesListModel> Choferes { get; set; }
    }
}