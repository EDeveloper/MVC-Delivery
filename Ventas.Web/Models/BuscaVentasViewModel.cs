﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class BuscaVentasViewModel
    {
        public BuscaVentasViewModel()
        {
            Ventas = new List<SeleccionarVentasViewModel>();
            //Ventas = new List<ListaVentasViewModel>();
            //SelectVentas = new List<SeleccionarVentasViewModel>();
        }
        public string Busqueda { get; set; }
        //public IEnumerable<ListaVentasViewModel> Ventas { get; set; }
        public IEnumerable<SeleccionarVentasViewModel> Ventas { get; set; }

        //public BuscaVentasViewModel()
        //{
        //    SelectVentas = new List<SeleccionarVentasViewModel>();
        //}
        //public IEnumerable<SeleccionarVentasViewModel> SelectVentas { get; set; }
    }
}