﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class VendedorSearchModel
    {
        public VendedorSearchModel()
        {
            Vendedores = new List<VendedorListModel>();
        }

        public string TipoBusqueda { get; set; }
        public string VendedoCodigo { get; set; }
        public string Busqueda { get; set; }
        public IEnumerable<VendedorListModel> Vendedores { get; set; }
    }
}