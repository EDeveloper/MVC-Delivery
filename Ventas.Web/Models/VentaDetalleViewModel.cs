﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
namespace Ventas.Web.Models
{
    public class VentaDetalleViewModel
    {
        public int VentaId { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaDespacho { get; set; }
        [Display(Description = "Cliente")]
        public string ClienteNombre { get; set; }
        [Display(Description = "Dirección")]
        public string ClienteDireccion { get; set; }
        public decimal MontoAfecto { get; set; }
        public decimal MontoNoAfecto { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal MontoTotal { get; set; }
        public string EstadoVenta { get; set; }
        public string EstadoDespacho { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public VentaCliente VentaCliente { get; set; }
        public IEnumerable<VentaDetalle> VentaDetalle { get; set; }
    }
    public class VentaCliente
    {
        public string ClienteNombre { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
    }

    public class VentaDetalle
    {
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public int Secuencia { get; set; }
        public string Producto { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Monto { get; set; }
        public string EstadoVenta { get; set; }
    }
}
