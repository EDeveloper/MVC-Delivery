﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ventas.Web.Models
{
    public class ZonasListModel
    {
        public int ZonaId { get; set; }
        public string ZonaCodigo { get; set; }
        public string Descripcion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        //public string Coordenadas { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
