﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ventas.Web.Models
{
    public class ListaVentasViewModel
    {
        public int VentaId { get; set; }
        public int ClienteId { get; set; } //solo por que uso EF
        public int DireccionId { get; set; } //solo por que uso EF
        public int VendedorId { get; set; } //solo por que uso EF
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaDespacho { get; set; }
        public string FormadePago { get; set; }
        public string EstadoDespacho { get; set; }
        public string MonedaDocumento { get; set; }
        public Decimal TipodeCambio { get; set; }
        public decimal MontoAfecto { get; set; }
        public decimal MontoNoAfecto { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal MontoTotal { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
