﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class BuscaClientesViewModel
    {
        public BuscaClientesViewModel()
        {
            Clientes = new List<ListaClientesViewModel>();
            Locations = new List<ListaLocationsViewModel>();
        }
        [Display(Name = "Cliente")]
        public string BusquedaCliente { get; set; }
        [Display(Name = "Documento")]
        public string BusquedaDocumento { get; set; }
        public IEnumerable<ListaClientesViewModel> Clientes { get; set; }
        public IEnumerable<ListaLocationsViewModel> Locations { get; set; }

    }
}