﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Ventas.Servicios.Dtos;

namespace Ventas.Web.Models
{
    public class EntregasViewModel
    {
        public EntregasViewModel(IEnumerable<VehiculoDto> vehiculos)
        {
            VehiculosLista = new SelectList(vehiculos, "VehiculoId", "NombreCompleto");
            FechaVenta = DateTime.Now;
            FechaRuta = DateTime.Now;
            if (FechaVenta.DayOfWeek == DayOfWeek.Friday)
            {
                FechaRuta = FechaRuta.AddDays(1);
            }
            
        }

        [Display(Description = "Vehiculos Disponibles", Name = "Vehiculos Disponibles")]
        public SelectList VehiculosLista { get; private set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaVenta { get; set; }

        public List<EntregasDto> Rutas { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaRuta { get; set; }
        public int VehiculoSeleccionado { get; set; }
        public decimal MontoTotal { get { return Rutas.Sum(x => x.MontoTotal); } }
    }


    public class EntregasGuardarViewModel
    {
        public List<EntregasDto> Rutas { get; set; }
        public int VehiculoSeleccionado { get; set; }
        public DateTime FechaRuta { get; set; }
    }

    public class EntregasRutasViewModel
    {
        public int VehiculoSeleccionado { get; set; }
        public DateTime FechaVenta { get; set; }
    }


    public class VisitasViewModel
    {
        public VisitasViewModel(IEnumerable<VendedorDto> vendedores, IEnumerable<ZonaDto> zonas)
        {
            VendedoresLista = new SelectList(vendedores, "VendedorId", "NombreCompleto");
            ZonasLista = new SelectList(zonas, "ZonaId", "ZonaCodigo");
        }

        [Display(Description = "Vendedores Disponibles", Name = "Vendedores Disponibles")]
        public SelectList VendedoresLista { get; private set; }


        public List<VisitasDto> Rutas { get; set; }

        public int VendedorSeleccionado { get; set; }

        public int ZonaSeleccionada { get; set; }
        [Display(Description = "Zonas Disponibles", Name = "Zonas Disponibles")]
        public SelectList ZonasLista { get; private set; }

    }

    public class VisitasGuardarViewModel
    {
        public List<VisitasDto> Rutas { get; set; }

        public int VendedorSeleccionado { get; set; }

        public int ZonaSeleccionada { get; set; }

    }

}
