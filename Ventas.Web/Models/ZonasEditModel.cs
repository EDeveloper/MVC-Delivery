﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace Ventas.Web.Models
{
    public class ZonasEditModel
    {
        public int ZonaId { get; set; }
        [Display(Name = "Zona")]
        public string ZonaCodigo { get; set; }
        public string Descripcion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        //public string Coordenadas { get; set; }
        public string Estado { get; set; }
        [Display(Name = "Usuario")]
        public string UltimoUsuario { get; set; }
        [Display(Name = "Fecha")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime UltimaFechaModif { get; set; }
    }
}
