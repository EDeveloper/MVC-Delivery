﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class BuscaVehiculosViewModel
    {
        public BuscaVehiculosViewModel()
        {
            Vehiculos = new List<ListaVehiculosViewModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<ListaVehiculosViewModel> Vehiculos { get; set; }
    }
}