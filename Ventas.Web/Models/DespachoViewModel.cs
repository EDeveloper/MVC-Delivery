﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class DespachoViewModel
    {

        public string  ClienteOrigen { get; set; }
        public string ClienteDestino { get; set; }

    }
}