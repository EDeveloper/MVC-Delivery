﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Ventas.Web.Models
{
    public class ClientesGeoListModel
    {
        public int ClienteId { get; set; }
        public int CodigoCliente { get; set; }
        public int DireccionId { get; set; }
        public int CodigoDireccion { get; set; }
        public string ClienteNombre { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string Direccion { get; set; }
        public string Estado { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string TipoCliente { get; set; }
    }
}
