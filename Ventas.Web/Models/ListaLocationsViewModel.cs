﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ventas.Web.Models
{
    public class ListaLocationsViewModel
    {

        public int DireccionId { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        //public int LocationId { get; set; }
        //public string LocationName { get; set; }
        //public double Latitude { get; set; }
        //public double Longitude { get; set; }

    }
}