﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using Ventas.Servicios.Dtos;
namespace Ventas.Web.Models
{
    public class VendedorListModel
    {
        public int VendedorId { get; set; }
        public int VendedorCodigo { get; set; }
        public string TipoVendedor { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string EquipoVenta { get; set; }
        public string FuerzaVenta { get; set; }
        public int Supervisor { get; set; }
        public string Avatar { get; set; }
        public Imagen AvatarIMG { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm}", ApplyFormatInEditMode = true)]
        public DateTime UltimaFechaModif { get; set; }
    }
}
