﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ventas.Web.Models
{
    public class BuscaDespachosViewModel
    {
        public BuscaDespachosViewModel()
        {
            Ventas = new List<ListaDespachosViewModel>();
        }
        public string Busqueda { get; set; }
        public IEnumerable<ListaDespachosViewModel> Ventas { get; set; }
    }
}