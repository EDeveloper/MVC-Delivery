﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ventas.Web.Models
{
    public class GeolocalizacionViewModel
    {
        public int ClienteId { get; set; }
        public int DireccionId { get; set; }
        [Required]
        public string Descripcion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string GeoFlag { get; set; }
        public int TipoCliente { get; set; }
        public SelectList Departamentos { get; set; }
        public SelectList Provincias { get; set; }
        public SelectList Distritos { get; set; }

        public GeolocalizacionViewModel()
        {
            var listaVacia = new List<string>() { "Seleccione ..." };
            Departamentos = new SelectList(listaVacia);
            Provincias = new SelectList(listaVacia);
            Distritos = new SelectList(listaVacia);
        }

    }
}