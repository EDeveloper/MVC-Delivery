﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ventas.Web.Models
{
    public class PapeletasSearchModel
    {
        public PapeletasSearchModel()
        {
            Papeletas = new List<PapeletasListModel>();
        }

        [Display(Description = "Chofer")]
        public string ChoferCodigo { get; set; }
        [Display(Description = "Vehiculo")]
        public string PlacaVehiculo { get; set; }
        //public string TipoInfraccion { get; set; }

        public SelectList Choferes { get; set; }
        public SelectList Vehiculos { get; set; }
        //public SelectList Infracciones { get; set; }
        public IEnumerable<PapeletasListModel> Papeletas { get; set; }
    }
}