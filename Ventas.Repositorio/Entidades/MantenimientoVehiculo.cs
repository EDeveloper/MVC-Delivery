﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class MantenimientoVehiculo : Entidad
    {
        public Vehiculo MentenimientoVehiculo { get; set; }
        public int VehiculoId { get; set; } //EF
        public DateTime FechaRegistro { get; set; }   
        public string Observaciones { get; set; }
        public DateTime FechaRetorno { get; set; }      
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }


    }
}
