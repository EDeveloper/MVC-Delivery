﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Venta : Entidad
    {
        public Venta()
        {
            VentaDetalle = new List<VentaDetalle>();
        }
        public virtual Cliente VentaCliente { get; set; }
        public int ClienteId { get; set; } //solo por que uso EF
        public virtual Direccion VentaDireccion { get; set; }
        public int DireccionId { get; set; } //solo por que uso EF
        public virtual Vendedor VentaVendedor { get; set; }
        public int VendedorId { get; set; } //solo por que uso EF
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public DateTime FechaDocumento { get; set; }
        public DateTime FechaDespacho { get; set; }
        public string FormadePago { get; set; }
        public string EstadoDespacho { get; set; }
        public string MonedaDocumento { get; set; }
        public Decimal TipodeCambio { get; set; }
        public decimal MontoAfecto { get; set; }
        public decimal MontoNoAfecto { get; set; }
        public decimal MontoImpuesto { get; set; }
        public decimal MontoTotal { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        //TODO : cambia este codigo para que obtenga el peso segun el nuevo modelo
        //public int PesoTotal { get { return VentaDetalle.Sum(v => v.Cantidad); } }
        public Decimal PesoTotal { get { return VentaDetalle.Sum(v => v.Peso); } }
        
        public virtual IList<VentaDetalle> VentaDetalle { get; set; }
        public void AddVentaDetalle(VentaDetalle ventaDetalle)
        {
            //EF asignar el Id y la instancia
            ventaDetalle.VentaId = this.Id;
            // NHibernate solo asigna la instancia del padre
            ventaDetalle.Venta = this;
            VentaDetalle.Add(ventaDetalle);
        }
    }
}
