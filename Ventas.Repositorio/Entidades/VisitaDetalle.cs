﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class VisitaDetalle : Entidad
    {
        public Visita Visita { get; set; }
        public int VisitaId { get; set; }
        public virtual Cliente ClienteaVisitar { get; set; }
        public int ClienteId { get; set; }
        public virtual Direccion DireccionaVisitar { get; set; }
        public int DireccionId { get; set; }
        //public string Zona { get; set; }
        public string DiaVisita { get; set; }
        public int VisitaSecuencia { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
