﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Cliente : Entidad
    {
        public Cliente()
        {
            Direcciones = new List<Direccion>();
        }
        public int ClienteCodigo { get; set; }
        public virtual TipoCliente TipodeCliente { get; set; }
        public int TipoClienteId { get; set; } //EF
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string ClienteNombre { get; set; }
        public string Direccion { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Estado { get; set; }
        public Decimal RatioVenta { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        public virtual IList<Direccion> Direcciones { get; set; }
        public void AddClienteDireccion(Direccion clienteDireccion)
        {
            //EF asignar el Id y la instancia
            clienteDireccion.ClienteId = this.Id;
            // NHibernate solo asigna la instancia del padre
            clienteDireccion.DireccionCliente = this;
            Direcciones.Add(clienteDireccion);
        }
    }
}
