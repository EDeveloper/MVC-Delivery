﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Disponibilidad : Entidad
    {
        public Chofer DisponibilidadChofer { get; set; } //EF
        public int ChoferId { get; set; } //EF
        public Vehiculo DisponibilidadVehiculo { get; set; } //EF
        public int VehiculoId { get; set; } //EF
        public DateTime FechaDisponibilidad { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
