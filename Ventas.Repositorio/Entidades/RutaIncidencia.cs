﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class RutaIncidencia : Entidad
    {
        public Ruta IncidenciaRuta { get; set; }
        public int RutaId { get; set; }
        public DateTime FechaIncidencia { get; set; }
        public string Descripcion { get; set; }
        public string Observacion { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
