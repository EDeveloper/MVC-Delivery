﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class RutaDetalle : Entidad
    {
        public Ruta Ruta { get; set; }
        public int RutaId { get; set; }
        public virtual Venta VentaaEntregar { get; set; }
        public int VentaId { get; set; }
        public int RutaSecuencia { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
