﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class TipoCliente : Entidad
    {
        public string TipoCodigo { get; set; }
        public string Descripcion { get; set; }
        public string Avatar { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
