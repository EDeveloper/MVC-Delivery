﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Vendedor : Entidad
    {
        public Vendedor()
        {
            VendedorZona = new List<VendedorZona>();
        }
        public int VendedorCodigo { get; set; }
        public string TipoVendedor { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string CorreoElectronico { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string EquipoVenta { get; set; }
        public string FuerzaVenta { get; set; }
        public int Supervisor { get; set; }
        public string Avatar { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
        public virtual IList<VendedorZona> VendedorZona { get; set; }
        public void AddVendedorZona(VendedorZona vendedorZona)
        {
            //EF asignar el Id y la instancia
            vendedorZona.VendedorId = this.Id;
            // NHibernate solo asigna la instancia del padre
            vendedorZona.Vendedor = this;
            VendedorZona.Add(vendedorZona);
        }
    }
}
