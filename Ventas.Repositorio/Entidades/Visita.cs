﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Visita : Entidad
    {
        public Visita()
        {
            VisitaDetalle = new List<VisitaDetalle>();
        }

        public virtual Vendedor VisitaVendedor { get; set; }
        public int VendedorId { get; set; } //solo por que uso EF
        public string CodigoVisita { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaVisita { get; set; }
        public string Observaciones { get; set; }       
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }

        public virtual IList<VisitaDetalle> VisitaDetalle { get; set; }
        public void AddVisitaDetalle(VisitaDetalle visitaDetalle)
        {
            //EF asignar el Id y la instancia
            visitaDetalle.VisitaId = this.Id;
            // NHibernate solo asigna la instancia del padre
            visitaDetalle.Visita = this;
            VisitaDetalle.Add(visitaDetalle);
        }
    }
}
