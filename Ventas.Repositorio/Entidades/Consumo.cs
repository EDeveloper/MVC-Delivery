﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Consumo : Entidad
    {
        public Chofer ConsumoChofer { get; set; } //EF
        public int ChoferId { get; set; } //EF
        public Vehiculo ConsumoVehiculo { get; set; } //EF
        public int VehiculoId { get; set; } //EF
        public Combustible Combustible { get; set; } //EF
        public int CombustibleId { get; set; } //EF
        public DateTime FechaConsumo { get; set; }
        public Decimal Cantidad { get; set; }
        public Decimal PrecioUnitario { get; set; }
        public Decimal MontoConsumo { get; set; }
        public string Establecimiento { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
