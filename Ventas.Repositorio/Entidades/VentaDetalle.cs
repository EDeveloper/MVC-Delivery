﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class VentaDetalle : Entidad
    {
        public Venta Venta { get; set; }
        public int VentaId { get; set; }
        public virtual Producto Producto { get; set; } //EF
        public int ProductoId { get; set; } //EF
        public string Lote { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal PrecioxCantidad { get; set; }
        public Decimal Peso { get { return Producto.Peso; } }
        public string TransferenciaGratuitaFlag { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }


    }
}
