﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Producto : Entidad
    {
        public string ProductoCodigo { get; set; }
        public string TipoProducto { get; set; }
        public string Descripcion { get; set; }
        public string UnidadMedida { get; set; }
        public string Linea { get; set; }
        public string Familia { get; set; }
        public string SubFamilia { get; set; }
        public string CodigodeBarra { get; set; }
        public Decimal  Peso { get; set; }
        public Decimal Largo { get; set; }
        public Decimal Ancho { get; set; }
        public Decimal Altura { get; set; }
        public string Avatar { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
