﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Ruta : Entidad
    {
        public Ruta()
        {
            RutaDetalle = new List<RutaDetalle>();
        }

        public virtual Chofer RutaChofer { get; set; }
        public int ChoferId { get; set; } //EF
        public virtual Vehiculo RutaVehiculo { get; set; }
        public int VehiculoId { get; set; } //EF
        public string CodigoRuta { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaDespacho { get; set; }
        public Decimal MontoTotal { get; set; }
        public string Observaciones { get; set; }       
        public string Estado { get; set; }
        public Decimal KilometrajeInicial { get; set; }
        public Decimal KilometrajeFinal { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }

        public virtual IList<RutaDetalle> RutaDetalle { get; set; }
        public void AddRutaDetalle(RutaDetalle rutaDetalle)
        {
            //EF asignar el Id y la instancia
            rutaDetalle.RutaId = this.Id;
            // NHibernate solo asigna la instancia del padre
            rutaDetalle.Ruta = this;
            RutaDetalle.Add(rutaDetalle);
        }
    }
}
