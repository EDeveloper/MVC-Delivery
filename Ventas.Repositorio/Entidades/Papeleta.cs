﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Papeleta : Entidad
    {
        public Chofer PapeletaChofer { get; set; } //EF
        public int ChoferId { get; set; } //EF
        public Vehiculo PapeletaVehiculo { get; set; } //EF
        public int VehiculoId { get; set; } //EF
        public string NumeroPapeleta { get; set; }
        public decimal MontoPapeleta { get; set; }
        public string Distrito { get; set; }
        public DateTime FechaPapeleta { get; set; }
        public string TipoInfraccion { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
