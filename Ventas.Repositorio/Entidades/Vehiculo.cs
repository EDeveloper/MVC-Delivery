﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class Vehiculo : Entidad
    {
        public virtual Chofer ChoferxDefecto { get; set; }
        public int ChoferId { get; set; } //EF
        public string PlacaVehiculo { get; set; }
        public string MarcaVehiculo { get; set; }
        public string Modelo { get; set; }
        public string  Color { get; set; }
        public string Descripcion { get; set; }
        public Decimal Capacidad { get; set; }
        public Decimal Largo { get; set; }
        public Decimal Ancho { get; set; }
        public Decimal Altura { get; set; }
        public string NumeroSoat  { get; set; }
        public DateTime FechaVencimientoSoat { get; set; }
        public string Anio { get; set; }
        public string Motor { get; set; }
        public string Chasis { get; set; }
        public Decimal KilometrajeActual { get; set; }
        public string Avatar { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }
    }
}
