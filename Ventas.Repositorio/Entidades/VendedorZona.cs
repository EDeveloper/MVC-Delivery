﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ventas.Repositorio.Entidades
{
    public class VendedorZona : Entidad
    {
        public virtual Vendedor Vendedor { get; set; } //EF
        public int VendedorId { get; set; } //EF
        public virtual Zona Zona { get; set; } //EF
        public int ZonaId { get; set; } //EF
        public string LunesFlag { get; set; }
        public string MartesFlag { get; set; }
        public string MiercolesFlag { get; set; }
        public string JuevesFlag { get; set; }
        public string ViernesFlag { get; set; }
        public string SabadoFlag { get; set; }
        public string DomingoFlag { get; set; }
        public string Estado { get; set; }
        public string UltimoUsuario { get; set; }
        public DateTime UltimaFechaModif { get; set; }

    }
}
