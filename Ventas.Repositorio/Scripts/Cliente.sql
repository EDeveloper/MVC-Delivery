-- DELETE FROM Cliente
INSERT INTO Cliente VALUES (
	'PHARMAMEDIC PERU S.R.L.                                                                             ',
	'RUC',
	'20463378089         ',
	NULL, 
	NULL,
	'AV 28 DE JULIO 2100 LA VICTORIA',
	'Activo',
	NULL,
	NULL,
	42477);
INSERT INTO Cliente VALUES (
	'INVERSIONES MOLAV S.A.C.                                                                            ',
	'RUC',
	'20551276113         ',
	NULL,
	NULL,
	'AV ALFREDO BENAVIDES 4862 SURCO',
	'Activo',
	NULL,
	NULL,
	105899);
INSERT INTO Cliente VALUES (
	'BOTICAS MINERVA                                                                                     ',
	'RUC',
	'20517359433         ',
	NULL,
	NULL,
	'AV ANGAMOS ESTE 784 SURQUILLO',
	'Activo',
	NULL,
	NULL,
	43171);
INSERT INTO Cliente VALUES (
	'INVERSIONES MAPI E.I.R.L.                                                                           ',
	'RUC',
	'20521707373         ',
	NULL,
	NULL,
	'AV ARIOSTO MATELLINI 609 URB MATELLINI',
	'Activo',
	NULL,
	NULL,
	48632);
INSERT INTO Cliente VALUES (
	'MINIMARKET DON MAX SRL                                                                              ',
	'RUC',
	'20154651587         ',
	NULL,
	NULL,
	'AV AVIACION 3969 URB CALERA DE LA MERCED',
	'Activo',
	NULL,
	NULL,
	109236);
INSERT INTO Cliente VALUES (
	'FERRETERIA RONCALLI                                                                                 ',
	'RUC',
	'20543204855         ',
	NULL,
	NULL,
	'AV AVIACION 4608 URB SAN ATANACIO DE PEDREGAL',
	'Activo',
	NULL,
	NULL,
	105474);
INSERT INTO Cliente VALUES (
	'GLORISA                                                                                             ',
	'RUC',
	'20515169742         ',
	NULL,
	NULL,
	'AV AYACUCHO 1287 URB LIGURIA',
	'Activo',
	NULL,
	NULL,
	21951);
INSERT INTO Cliente VALUES (
	'BOTICA FARMACOM                                                                                     ',
	'RUC',
	'20504615643         ',
	NULL,
	NULL,
	'AV AYACUCHO 1351 URB LIGURIA SURCO',
	'Activo',
	NULL,
	NULL,
	95069);
INSERT INTO Cliente VALUES (
	'GOLDYPAN S.C.R.L                                                                                    ',
	'RUC',
	'20502457722         ',
	NULL,
	NULL,
	'AV AYACUCHO 198 SURCO',
	'Activo',
	NULL,
	NULL,
	48288);
INSERT INTO Cliente VALUES (
	'E&R FLORES GENERAL SERVICE S.A.C.                                                                   ',
	'RUC',
	'20510776284         ',
	NULL,
	NULL,
	'AV BENAVIDES 3601 STAND 198 2DO PISO ',
	'Activo',
	NULL,
	NULL,
	59464);
INSERT INTO Cliente VALUES (
	'CHANTILLI SA                                                                                        ',
	'RUC',
	'20127318761         ',
	NULL,
	NULL,
	'AV BENAVIDES 5160 URB LAS GARDENIAS SANTIAGO DE SURCO',
	'Activo',
	NULL,
	NULL,
	96745);
INSERT INTO Cliente VALUES (
	'PRINT TEC E.I.R.L.                                                                                  ',
	'RUC',
	'20518205171         ',
	NULL,
	NULL,
	'AV BENAVIDES 5403 STAND 1',
	'Activo',
	NULL,
	NULL,
	98636);
INSERT INTO Cliente VALUES (
	'LE PONT D OR SCRL                                                                                   ',
	'RUC',
	'20505098006         ',
	NULL,
	NULL,
	'AV BENAVIDES 5471 URB GARDENIAS',
	'Activo',
	NULL,
	NULL,
	28352);
INSERT INTO Cliente VALUES (
	'CONGREGACION DE LAS HERMANITAS DE LOS ANCIANOS DESAMPARADOS                                         ',
	'RUC',
	'20170715056         ',
	NULL,
	NULL,
	'AV BRASIL 496 BRENA',
	'Activo',
	NULL,
	NULL,
	43090);
INSERT INTO Cliente VALUES (
	'COLOMBI ASOCIADOS SAC                                                                               ',
	'RUC',
	'20102081731         ',
	NULL,
	NULL,
	'AV CAMINOS DEL INCA 1092',
	'Activo',
	NULL,
	NULL,
	30103);
INSERT INTO Cliente VALUES (
	'P.P.A. ALEX S.A.C                                                                                   ',
	'RUC',
	'20509777579         ',
	NULL,
	NULL,
	'AV CAMINOS DEL INCA 1506',
	'Activo',
	NULL,
	NULL,
	30780);
INSERT INTO Cliente VALUES (
	'INVERSIONES FAUSTINO S.A.C                                                                          ',
	'RUC',
	'20514166278         ',
	NULL,
	NULL,
	'AV CAMINOS DEL INCA 1538  URB LAS GARDENIAS  SANTIAGO DE SURC',
	'Activo',
	NULL,
	NULL,
	96275);
INSERT INTO Cliente VALUES (
	'PANIFICADORA LA NUEZ SAC                                                                            ',
	'RUC',
	'20392808761         ',
	NULL,
	NULL,
	'AV CAMINOS DEL INCA 2897',
	'Activo',
	NULL,
	NULL,
	97438);
INSERT INTO Cliente VALUES (
	'PASTELERIA FINA GMR SOCIEDAD ANONIMA CERRADA                                                        ',
	'RUC',
	'20523318978         ',
	NULL,
	NULL,
	'AV CAMINOS DEL INCA 590',
	'Activo',
	NULL,
	NULL,
	102431);
INSERT INTO Cliente VALUES (
	'COMERCIAL DONA VICKI EIRL.                                                                          ',
	'RUC',
	'20508043563         ',
	NULL,
	'0',
	'AV CAMINOS DEL INCA 803',
	'Activo',
	NULL,
	NULL,
	24643);



GO

 Insert into Direccion
 Select Direccion,null,null,null,'Activo',null,null, Id from Cliente

