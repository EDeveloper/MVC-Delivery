﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VisitaRepositorio : GenericRepository<EFOlimpoRepository, Visita>
    {
        public VisitaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}