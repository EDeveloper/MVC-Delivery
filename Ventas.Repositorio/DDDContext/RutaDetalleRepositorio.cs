﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class RutaDetalleRepositorio : GenericRepository<EFOlimpoRepository, RutaDetalle>
    {
        public RutaDetalleRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}