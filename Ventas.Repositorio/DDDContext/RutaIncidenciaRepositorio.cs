﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class RutaIncidenciaRepositorio : GenericRepository<EFOlimpoRepository, RutaIncidencia>
    {
        public RutaIncidenciaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}