﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class MantenimientoVehiculoRepositorio : GenericRepository<EFOlimpoRepository, MantenimientoVehiculo>
    {
        public MantenimientoVehiculoRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}