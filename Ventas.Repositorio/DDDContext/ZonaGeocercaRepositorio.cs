﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class ZonaGeocercaRepositorio : GenericRepository<EFOlimpoRepository,ZonaGeocerca>
    {
        public ZonaGeocercaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}