﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class CombustibleRepositorio : GenericRepository<EFOlimpoRepository,Combustible>
    {
        public CombustibleRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}