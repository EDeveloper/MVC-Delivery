﻿using System;
using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public interface IOlimpoRepository : IDisposable
    {
        IGenericRepository<Chofer> ChoferRepository { get; }
        IGenericRepository<Cliente> ClienteRepository { get; }
        IGenericRepository<Combustible> CombustibleRepository { get; }
        IGenericRepository<Consumo> ConsumoRepository { get; }
        IGenericRepository<Direccion> DireccionRepository { get; }
        IGenericRepository<Disponibilidad> DisponibilidadRepository { get; }
        IGenericRepository<MantenimientoVehiculo> MantenimientoVehiculoRepository { get; }
        IGenericRepository<Papeleta> PapeletaRepository { get; }
        IGenericRepository<Producto> ProductoRepository { get; }
        IGenericRepository<RutaDetalle> RutaDetalleRepository { get; }
        IGenericRepository<RutaIncidencia> RutaIncidenciaRepository { get; }
        IGenericRepository<Ruta> RutaRepository { get; }
        IGenericRepository<TipoCliente> TipoClienteRepository { get; }
        IGenericRepository<Vehiculo> VehiculoRepository { get; }
        IGenericRepository<Vendedor> VendedorRepository { get; }
        IGenericRepository<VendedorZona> VendedorZonaRepository { get; }
        IGenericRepository<VentaDetalle> VentaDetalleRepository { get; }
        IGenericRepository<Venta> VentaRepository { get; }
        IGenericRepository<VisitaDetalle> VisitaDetalleRepository { get; }
        IGenericRepository<Visita> VisitaRepository { get; }
        IGenericRepository<Zona> ZonaRepository { get; }
        IGenericRepository<ZonaGeocerca> ZonaGeocercaRepository { get; }
        void Commit();
    }
}