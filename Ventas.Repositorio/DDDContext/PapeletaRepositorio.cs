﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class PapeletaRepositorio : GenericRepository<EFOlimpoRepository,Papeleta>
    {
        public PapeletaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}