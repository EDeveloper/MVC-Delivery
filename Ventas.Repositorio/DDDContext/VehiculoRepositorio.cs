﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VehiculoRepositorio : GenericRepository<EFOlimpoRepository,Vehiculo>
    {
        public VehiculoRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}