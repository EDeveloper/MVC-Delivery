﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VentaRepositorio : GenericRepository<EFOlimpoRepository, Venta>
    {
        public VentaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}