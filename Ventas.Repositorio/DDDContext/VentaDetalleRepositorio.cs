﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VentaDetalleRepositorio : GenericRepository<EFOlimpoRepository, VentaDetalle>
    {
        public VentaDetalleRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}