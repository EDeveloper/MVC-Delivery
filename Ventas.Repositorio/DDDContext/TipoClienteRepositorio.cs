﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class TipoClienteRepositorio : GenericRepository<EFOlimpoRepository, TipoCliente>
    {
        public TipoClienteRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}