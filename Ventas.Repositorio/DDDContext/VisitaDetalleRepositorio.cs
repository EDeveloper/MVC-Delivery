﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VisitaDetalleRepositorio : GenericRepository<EFOlimpoRepository, VisitaDetalle>
    {
        public VisitaDetalleRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}