﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class EFOlimpoRepository : DbContext, IOlimpoRepository
    {
        private readonly ChoferRepositorio _choferRepo;
        private readonly ClienteRepositorio _clienteRepo;
        private readonly CombustibleRepositorio _combustibleRepo;
        private readonly ConsumoRepositorio _consumoRepo;
        private readonly DireccionRepositorio _direccionRepo;
        private readonly DisponibilidadRepositorio _disponibilidadRepo;
        private readonly MantenimientoVehiculoRepositorio _mantVehiculoRepo;
        private readonly PapeletaRepositorio _papeletaRepo;
        private readonly ProductoRepositorio _productoRepo;
        private readonly RutaDetalleRepositorio _rutaDetRepo;
        private readonly RutaIncidenciaRepositorio _rutaIncidenciaRepo;
        private readonly RutaRepositorio _rutaRepo;
        private readonly TipoClienteRepositorio _tipoClienteRepo;
        private readonly VehiculoRepositorio _vehiculoRepo;
        private readonly VendedorRepositorio _vendedorRepo;
        private readonly VendedorZonaRepositorio _vendedorZonaRepo;
        private readonly VentaDetalleRepositorio _ventaDetRepo;
        private readonly VentaRepositorio _ventaRepo;
        private readonly VisitaDetalleRepositorio _visitaDetRepo;
        private readonly VisitaRepositorio _visitaRepo;
        private readonly ZonaRepositorio _zonaRepo;
        private readonly ZonaGeocercaRepositorio _zonaGeoRepo;
        public DbSet<Chofer> Choferes { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Combustible> Combustibles { get; set; }
        public DbSet<Consumo> Consumos { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }
        public DbSet<Disponibilidad> Disponibilidades { get; set; }
        public DbSet<MantenimientoVehiculo> MantenimientoVehiculos { get; set; }
        public DbSet<Papeleta> Papeletas { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<RutaDetalle> RutaDetalle { get; set; }
        public DbSet<RutaIncidencia> RutaIncidencias { get; set; }
        public DbSet<Ruta> Rutas { get; set; }
        public DbSet<TipoCliente> TipoClientes { get; set; }
        public DbSet<Vehiculo> Vehiculos { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<VendedorZona> VendedorZonas { get; set; }
        public DbSet<VentaDetalle> VentasDetalle { get; set; }
        public DbSet<Venta> Ventas { get; set; }
        public DbSet<VisitaDetalle> VisitaDetalle { get; set; }
        public DbSet<Visita> Visitas { get; set; }
        public DbSet<Zona> Zonas { get; set; }
        public DbSet<ZonaGeocerca> ZonasGeocerca { get; set; }
        public EFOlimpoRepository()
            : base("name=OlimpoDB")
        {
            _choferRepo = new ChoferRepositorio(this);
            _clienteRepo = new ClienteRepositorio(this);
            _combustibleRepo = new CombustibleRepositorio(this);
            _consumoRepo = new ConsumoRepositorio(this);
            _direccionRepo = new DireccionRepositorio(this);
            _disponibilidadRepo = new DisponibilidadRepositorio(this);
            _mantVehiculoRepo = new MantenimientoVehiculoRepositorio(this);
            _papeletaRepo = new PapeletaRepositorio(this);
            _productoRepo = new ProductoRepositorio(this);
            _rutaDetRepo = new RutaDetalleRepositorio(this);
            _rutaIncidenciaRepo = new RutaIncidenciaRepositorio(this);
            _rutaRepo = new RutaRepositorio(this); ;
            _tipoClienteRepo = new TipoClienteRepositorio(this);
            _vehiculoRepo = new VehiculoRepositorio(this);
            _vendedorRepo = new VendedorRepositorio(this);
            _vendedorZonaRepo = new VendedorZonaRepositorio(this);
            _ventaDetRepo = new VentaDetalleRepositorio(this);
            _ventaRepo = new VentaRepositorio(this);
            _visitaDetRepo = new VisitaDetalleRepositorio(this);
            _visitaRepo = new VisitaRepositorio(this);
            _zonaRepo = new ZonaRepositorio(this);
            _zonaGeoRepo = new ZonaGeocercaRepositorio(this);
        }

        #region IUnitOfWork Implementation



        public void Commit()
        {
            this.SaveChanges();
        }

        #endregion

        public IGenericRepository<Chofer> ChoferRepository
        {
            get { return _choferRepo; }
        }
        public IGenericRepository<Cliente> ClienteRepository
        {
            get { return _clienteRepo; }
        }
        public IGenericRepository<Combustible> CombustibleRepository
        {
            get { return _combustibleRepo; }
        }
        public IGenericRepository<Consumo> ConsumoRepository
        {
            get { return _consumoRepo; }
        }

        public IGenericRepository<Direccion> DireccionRepository
        {
            get { return _direccionRepo; }
        }
        public IGenericRepository<Disponibilidad> DisponibilidadRepository
        {
            get { return _disponibilidadRepo; }
        }
        

        public IGenericRepository<MantenimientoVehiculo> MantenimientoVehiculoRepository
        {
            get { return _mantVehiculoRepo; }
        }

        public IGenericRepository<Papeleta> PapeletaRepository
        {
            get { return _papeletaRepo; }
        }


        public IGenericRepository<Producto> ProductoRepository
        {
            get { return _productoRepo; }
        }

        public IGenericRepository<RutaDetalle> RutaDetalleRepository
        {
            get { return _rutaDetRepo; }
        }
        public IGenericRepository<RutaIncidencia> RutaIncidenciaRepository
        {
            get { return _rutaIncidenciaRepo; }
        }
        public IGenericRepository<Ruta> RutaRepository
        {
            get { return _rutaRepo; }
        }
        public IGenericRepository<TipoCliente> TipoClienteRepository
        {
            get { return _tipoClienteRepo; }
        }
        public IGenericRepository<Vehiculo> VehiculoRepository
        {
            get { return _vehiculoRepo; }
        }
        public IGenericRepository<Vendedor> VendedorRepository
        {
            get { return _vendedorRepo; }
        }
        public IGenericRepository<VendedorZona> VendedorZonaRepository
        {
            get { return _vendedorZonaRepo; }
        }
        public IGenericRepository<VentaDetalle> VentaDetalleRepository
        {
            get { return _ventaDetRepo; }
        }
        public IGenericRepository<Venta> VentaRepository
        {
            get { return _ventaRepo; }
        }

        public IGenericRepository<VisitaDetalle> VisitaDetalleRepository
        {
            get { return _visitaDetRepo; }
        }
        public IGenericRepository<Visita> VisitaRepository
        {
            get { return _visitaRepo; }
        }

        public IGenericRepository<Zona> ZonaRepository
        {
            get { return _zonaRepo; }
        }

        public IGenericRepository<ZonaGeocerca> ZonaGeocercaRepository
        {
            get { return _zonaGeoRepo; }
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}