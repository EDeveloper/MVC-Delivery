﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VendedorZonaRepositorio : GenericRepository<EFOlimpoRepository, VendedorZona>
    {
        public VendedorZonaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}