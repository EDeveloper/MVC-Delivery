﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class DireccionRepositorio : GenericRepository<EFOlimpoRepository, Direccion>
    {
        public DireccionRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}