﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class VendedorRepositorio : GenericRepository<EFOlimpoRepository,Vendedor>
    {
        public VendedorRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}