﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class ConsumoRepositorio : GenericRepository<EFOlimpoRepository,Consumo>
    {
        public ConsumoRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}