﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class ChoferRepositorio : GenericRepository<EFOlimpoRepository,Chofer>
    {
        public ChoferRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}