﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class ProductoRepositorio : GenericRepository<EFOlimpoRepository, Producto>
    {
        public ProductoRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}