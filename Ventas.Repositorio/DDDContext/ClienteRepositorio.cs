﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class ClienteRepositorio : GenericRepository<EFOlimpoRepository,Cliente>
    {
        public ClienteRepositorio(EFOlimpoRepository context) : base(context)
        {
        }
    }
}