﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class DisponibilidadRepositorio : GenericRepository<EFOlimpoRepository, Disponibilidad>
    {
        public DisponibilidadRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}