﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class ZonaRepositorio : GenericRepository<EFOlimpoRepository,Zona>
    {
        public ZonaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}