﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio.DDDContext
{
    public class RutaRepositorio : GenericRepository<EFOlimpoRepository, Ruta>
    {
        public RutaRepositorio(EFOlimpoRepository context)
            : base(context)
        {
        }
    }
}