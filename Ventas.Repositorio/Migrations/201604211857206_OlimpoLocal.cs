namespace Ventas.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OlimpoLocal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chofer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoferCodigo = c.String(),
                        Nombres = c.String(),
                        Apellidos = c.String(),
                        TipoDocumento = c.String(),
                        NumeroDocumento = c.String(),
                        Direccion = c.String(),
                        Brevete = c.String(),
                        CategoriaBrevete = c.String(),
                        FechaVencimiento = c.DateTime(nullable: false),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteCodigo = c.Int(nullable: false),
                        TipoClienteId = c.Int(nullable: false),
                        TipoDocumento = c.String(),
                        NumeroDocumento = c.String(),
                        ClienteNombre = c.String(),
                        Direccion = c.String(),
                        CorreoElectronico = c.String(),
                        Telefono = c.String(),
                        Estado = c.String(),
                        RatioVenta = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.TipoCliente", t => t.TipoClienteId)
                .Index(t => t.TipoClienteId);
            
            CreateTable(
                "dbo.Direccion",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        ZonaId = c.Int(nullable: false),
                        ClienteCodigo = c.Int(nullable: false),
                        DireccionCodigo = c.Int(nullable: false),
                        Descripcion = c.String(),
                        Departamento = c.String(),
                        Provincia = c.String(),
                        Distrito = c.String(),
                        Latitud = c.String(),
                        Longitud = c.String(),
                        GeoFlag = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cliente", t => t.ClienteId)
                .ForeignKey("dbo.Zona", t => t.ZonaId)
                .Index(t => t.ClienteId)
                .Index(t => t.ZonaId);
            
            CreateTable(
                "dbo.Zona",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ZonaCodigo = c.String(),
                        Descripcion = c.String(),
                        Departamento = c.String(),
                        Provincia = c.String(),
                        Distrito = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TipoCliente",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TipoCodigo = c.String(),
                        Descripcion = c.String(),
                        Avatar = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Combustible",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CombustibleCodigo = c.String(),
                        Descripcion = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Consumo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoferId = c.Int(nullable: false),
                        VehiculoId = c.Int(nullable: false),
                        CombustibleId = c.Int(nullable: false),
                        FechaConsumo = c.DateTime(nullable: false),
                        Cantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MontoConsumo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Establecimiento = c.String(),
                        Latitud = c.String(),
                        Longitud = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Combustible", t => t.CombustibleId)
                .ForeignKey("dbo.Chofer", t => t.ChoferId)
                .ForeignKey("dbo.Vehiculo", t => t.VehiculoId)
                .Index(t => t.ChoferId)
                .Index(t => t.VehiculoId)
                .Index(t => t.CombustibleId);
            
            CreateTable(
                "dbo.Vehiculo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoferId = c.Int(nullable: false),
                        PlacaVehiculo = c.String(),
                        MarcaVehiculo = c.String(),
                        Modelo = c.String(),
                        Color = c.String(),
                        Descripcion = c.String(),
                        Capacidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Largo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ancho = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Altura = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NumeroSoat = c.String(),
                        FechaVencimientoSoat = c.DateTime(nullable: false),
                        Anio = c.String(),
                        Motor = c.String(),
                        Chasis = c.String(),
                        KilometrajeActual = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Avatar = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chofer", t => t.ChoferId)
                .Index(t => t.ChoferId);
            
            CreateTable(
                "dbo.Disponibilidad",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoferId = c.Int(nullable: false),
                        VehiculoId = c.Int(nullable: false),
                        FechaDisponibilidad = c.DateTime(nullable: false),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chofer", t => t.ChoferId)
                .ForeignKey("dbo.Vehiculo", t => t.VehiculoId)
                .Index(t => t.ChoferId)
                .Index(t => t.VehiculoId);
            
            CreateTable(
                "dbo.MantenimientoVehiculo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VehiculoId = c.Int(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        Observaciones = c.String(),
                        FechaRetorno = c.DateTime(nullable: false),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vehiculo", t => t.VehiculoId)
                .Index(t => t.VehiculoId);
            
            CreateTable(
                "dbo.Papeleta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoferId = c.Int(nullable: false),
                        VehiculoId = c.Int(nullable: false),
                        NumeroPapeleta = c.String(),
                        MontoPapeleta = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Distrito = c.String(),
                        FechaPapeleta = c.DateTime(nullable: false),
                        TipoInfraccion = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chofer", t => t.ChoferId)
                .ForeignKey("dbo.Vehiculo", t => t.VehiculoId)
                .Index(t => t.ChoferId)
                .Index(t => t.VehiculoId);
            
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ProductoCodigo = c.String(),
                        TipoProducto = c.String(),
                        Descripcion = c.String(),
                        UnidadMedida = c.String(),
                        Linea = c.String(),
                        Familia = c.String(),
                        SubFamilia = c.String(),
                        CodigodeBarra = c.String(),
                        Peso = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Largo = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Ancho = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Altura = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Avatar = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RutaDetalle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RutaId = c.Int(nullable: false),
                        VentaId = c.Int(nullable: false),
                        RutaSecuencia = c.Int(nullable: false),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ruta", t => t.RutaId)
                .ForeignKey("dbo.Venta", t => t.VentaId)
                .Index(t => t.RutaId)
                .Index(t => t.VentaId);
            
            CreateTable(
                "dbo.Ruta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ChoferId = c.Int(nullable: false),
                        VehiculoId = c.Int(nullable: false),
                        CodigoRuta = c.String(),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaDespacho = c.DateTime(nullable: false),
                        MontoTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Observaciones = c.String(),
                        Estado = c.String(),
                        KilometrajeInicial = c.Decimal(nullable: false, precision: 18, scale: 2),
                        KilometrajeFinal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chofer", t => t.ChoferId)
                .ForeignKey("dbo.Vehiculo", t => t.VehiculoId)
                .Index(t => t.ChoferId)
                .Index(t => t.VehiculoId);
            
            CreateTable(
                "dbo.Venta",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClienteId = c.Int(nullable: false),
                        DireccionId = c.Int(nullable: false),
                        VendedorId = c.Int(nullable: false),
                        TipoDocumento = c.String(),
                        NumeroDocumento = c.String(),
                        FechaDocumento = c.DateTime(nullable: false),
                        FechaDespacho = c.DateTime(nullable: false),
                        FormadePago = c.String(),
                        EstadoDespacho = c.String(),
                        MonedaDocumento = c.String(),
                        TipodeCambio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MontoAfecto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MontoNoAfecto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MontoImpuesto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MontoTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cliente", t => t.ClienteId)
                .ForeignKey("dbo.Direccion", t => t.DireccionId)
                .ForeignKey("dbo.Vendedor", t => t.VendedorId)
                .Index(t => t.ClienteId)
                .Index(t => t.DireccionId)
                .Index(t => t.VendedorId);
            
            CreateTable(
                "dbo.VentaDetalle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VentaId = c.Int(nullable: false),
                        ProductoId = c.Int(nullable: false),
                        Lote = c.String(),
                        Cantidad = c.Int(nullable: false),
                        PrecioUnitario = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrecioxCantidad = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TransferenciaGratuitaFlag = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Producto", t => t.ProductoId)
                .ForeignKey("dbo.Venta", t => t.VentaId)
                .Index(t => t.VentaId)
                .Index(t => t.ProductoId);
            
            CreateTable(
                "dbo.Vendedor",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendedorCodigo = c.Int(nullable: false),
                        TipoVendedor = c.String(),
                        Nombres = c.String(),
                        Apellidos = c.String(),
                        TipoDocumento = c.String(),
                        NumeroDocumento = c.String(),
                        CorreoElectronico = c.String(),
                        Telefono = c.String(),
                        Direccion = c.String(),
                        EquipoVenta = c.String(),
                        FuerzaVenta = c.String(),
                        Supervisor = c.Int(nullable: false),
                        Avatar = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.VendedorZona",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendedorId = c.Int(nullable: false),
                        ZonaId = c.Int(nullable: false),
                        LunesFlag = c.String(),
                        MartesFlag = c.String(),
                        MiercolesFlag = c.String(),
                        JuevesFlag = c.String(),
                        ViernesFlag = c.String(),
                        SabadoFlag = c.String(),
                        DomingoFlag = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vendedor", t => t.VendedorId)
                .ForeignKey("dbo.Zona", t => t.ZonaId)
                .Index(t => t.VendedorId)
                .Index(t => t.ZonaId);
            
            CreateTable(
                "dbo.RutaIncidencia",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RutaId = c.Int(nullable: false),
                        FechaIncidencia = c.DateTime(nullable: false),
                        Descripcion = c.String(),
                        Observacion = c.String(),
                        Latitud = c.String(),
                        Longitud = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ruta", t => t.RutaId)
                .Index(t => t.RutaId);
            
            CreateTable(
                "dbo.VisitaDetalle",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VisitaId = c.Int(nullable: false),
                        ClienteId = c.Int(nullable: false),
                        DireccionId = c.Int(nullable: false),
                        DiaVisita = c.String(),
                        VisitaSecuencia = c.Int(nullable: false),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cliente", t => t.ClienteId)
                .ForeignKey("dbo.Direccion", t => t.DireccionId)
                .ForeignKey("dbo.Visita", t => t.VisitaId)
                .Index(t => t.VisitaId)
                .Index(t => t.ClienteId)
                .Index(t => t.DireccionId);
            
            CreateTable(
                "dbo.Visita",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        VendedorId = c.Int(nullable: false),
                        CodigoVisita = c.String(),
                        FechaRegistro = c.DateTime(nullable: false),
                        FechaVisita = c.DateTime(nullable: false),
                        Observaciones = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Vendedor", t => t.VendedorId)
                .Index(t => t.VendedorId);
            
            CreateTable(
                "dbo.ZonaGeocerca",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ZonaId = c.Int(nullable: false),
                        Secuencia = c.Int(nullable: false),
                        Latitud = c.String(),
                        Longitud = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Zona", t => t.ZonaId)
                .Index(t => t.ZonaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZonaGeocerca", "ZonaId", "dbo.Zona");
            DropForeignKey("dbo.Visita", "VendedorId", "dbo.Vendedor");
            DropForeignKey("dbo.VisitaDetalle", "VisitaId", "dbo.Visita");
            DropForeignKey("dbo.VisitaDetalle", "DireccionId", "dbo.Direccion");
            DropForeignKey("dbo.VisitaDetalle", "ClienteId", "dbo.Cliente");
            DropForeignKey("dbo.RutaIncidencia", "RutaId", "dbo.Ruta");
            DropForeignKey("dbo.RutaDetalle", "VentaId", "dbo.Venta");
            DropForeignKey("dbo.Venta", "VendedorId", "dbo.Vendedor");
            DropForeignKey("dbo.VendedorZona", "ZonaId", "dbo.Zona");
            DropForeignKey("dbo.VendedorZona", "VendedorId", "dbo.Vendedor");
            DropForeignKey("dbo.Venta", "DireccionId", "dbo.Direccion");
            DropForeignKey("dbo.VentaDetalle", "VentaId", "dbo.Venta");
            DropForeignKey("dbo.VentaDetalle", "ProductoId", "dbo.Producto");
            DropForeignKey("dbo.Venta", "ClienteId", "dbo.Cliente");
            DropForeignKey("dbo.Ruta", "VehiculoId", "dbo.Vehiculo");
            DropForeignKey("dbo.RutaDetalle", "RutaId", "dbo.Ruta");
            DropForeignKey("dbo.Ruta", "ChoferId", "dbo.Chofer");
            DropForeignKey("dbo.Papeleta", "VehiculoId", "dbo.Vehiculo");
            DropForeignKey("dbo.Papeleta", "ChoferId", "dbo.Chofer");
            DropForeignKey("dbo.MantenimientoVehiculo", "VehiculoId", "dbo.Vehiculo");
            DropForeignKey("dbo.Disponibilidad", "VehiculoId", "dbo.Vehiculo");
            DropForeignKey("dbo.Disponibilidad", "ChoferId", "dbo.Chofer");
            DropForeignKey("dbo.Consumo", "VehiculoId", "dbo.Vehiculo");
            DropForeignKey("dbo.Vehiculo", "ChoferId", "dbo.Chofer");
            DropForeignKey("dbo.Consumo", "ChoferId", "dbo.Chofer");
            DropForeignKey("dbo.Consumo", "CombustibleId", "dbo.Combustible");
            DropForeignKey("dbo.Cliente", "TipoClienteId", "dbo.TipoCliente");
            DropForeignKey("dbo.Direccion", "ZonaId", "dbo.Zona");
            DropForeignKey("dbo.Direccion", "ClienteId", "dbo.Cliente");
            DropIndex("dbo.ZonaGeocerca", new[] { "ZonaId" });
            DropIndex("dbo.Visita", new[] { "VendedorId" });
            DropIndex("dbo.VisitaDetalle", new[] { "DireccionId" });
            DropIndex("dbo.VisitaDetalle", new[] { "ClienteId" });
            DropIndex("dbo.VisitaDetalle", new[] { "VisitaId" });
            DropIndex("dbo.RutaIncidencia", new[] { "RutaId" });
            DropIndex("dbo.VendedorZona", new[] { "ZonaId" });
            DropIndex("dbo.VendedorZona", new[] { "VendedorId" });
            DropIndex("dbo.VentaDetalle", new[] { "ProductoId" });
            DropIndex("dbo.VentaDetalle", new[] { "VentaId" });
            DropIndex("dbo.Venta", new[] { "VendedorId" });
            DropIndex("dbo.Venta", new[] { "DireccionId" });
            DropIndex("dbo.Venta", new[] { "ClienteId" });
            DropIndex("dbo.Ruta", new[] { "VehiculoId" });
            DropIndex("dbo.Ruta", new[] { "ChoferId" });
            DropIndex("dbo.RutaDetalle", new[] { "VentaId" });
            DropIndex("dbo.RutaDetalle", new[] { "RutaId" });
            DropIndex("dbo.Papeleta", new[] { "VehiculoId" });
            DropIndex("dbo.Papeleta", new[] { "ChoferId" });
            DropIndex("dbo.MantenimientoVehiculo", new[] { "VehiculoId" });
            DropIndex("dbo.Disponibilidad", new[] { "VehiculoId" });
            DropIndex("dbo.Disponibilidad", new[] { "ChoferId" });
            DropIndex("dbo.Vehiculo", new[] { "ChoferId" });
            DropIndex("dbo.Consumo", new[] { "CombustibleId" });
            DropIndex("dbo.Consumo", new[] { "VehiculoId" });
            DropIndex("dbo.Consumo", new[] { "ChoferId" });
            DropIndex("dbo.Direccion", new[] { "ZonaId" });
            DropIndex("dbo.Direccion", new[] { "ClienteId" });
            DropIndex("dbo.Cliente", new[] { "TipoClienteId" });
            DropTable("dbo.ZonaGeocerca");
            DropTable("dbo.Visita");
            DropTable("dbo.VisitaDetalle");
            DropTable("dbo.RutaIncidencia");
            DropTable("dbo.VendedorZona");
            DropTable("dbo.Vendedor");
            DropTable("dbo.VentaDetalle");
            DropTable("dbo.Venta");
            DropTable("dbo.Ruta");
            DropTable("dbo.RutaDetalle");
            DropTable("dbo.Producto");
            DropTable("dbo.Papeleta");
            DropTable("dbo.MantenimientoVehiculo");
            DropTable("dbo.Disponibilidad");
            DropTable("dbo.Vehiculo");
            DropTable("dbo.Consumo");
            DropTable("dbo.Combustible");
            DropTable("dbo.TipoCliente");
            DropTable("dbo.Zona");
            DropTable("dbo.Direccion");
            DropTable("dbo.Cliente");
            DropTable("dbo.Chofer");
        }
    }
}
