namespace Ventas.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OlimpoCambiosForzados : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Mantenimiento", "VehiculoId", "dbo.Vehiculo");
            DropIndex("dbo.Mantenimiento", new[] { "VehiculoId" });
            //DropTable("dbo.Mantenimiento");
        }
        
        public override void Down()
        {
            //CreateTable(
            //    "dbo.Mantenimiento",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            VehiculoId = c.Int(nullable: false),
            //            Fecha = c.DateTime(nullable: false),
            //            TipoMantenimiento = c.String(),
            //            Observacion = c.String(),
            //            KilometrajeActual = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            Estado = c.String(),
            //            UltimoUsuario = c.String(),
            //            UltimaFechaModif = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateIndex("dbo.Mantenimiento", "VehiculoId");
            //AddForeignKey("dbo.Mantenimiento", "VehiculoId", "dbo.Vehiculo", "Id");
        }
    }
}
