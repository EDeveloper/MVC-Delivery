namespace Ventas.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OlimpoDB_correcciones : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.RutaIncidencia", "ChoferId", "dbo.Chofer");
            DropForeignKey("dbo.RutaIncidencia", "VehiculoId", "dbo.Vehiculo");
            DropIndex("dbo.RutaIncidencia", new[] { "ChoferId" });
            DropIndex("dbo.RutaIncidencia", new[] { "VehiculoId" });
            AddColumn("dbo.Consumo", "Cantidad", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Consumo", "PrecioUnitario", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Vehiculo", "FechaVencimientoSoat", c => c.DateTime(nullable: false));
            AddColumn("dbo.MantenimientoVehiculo", "FechaRegistro", c => c.DateTime(nullable: false));
            AddColumn("dbo.RutaIncidencia", "RutaId", c => c.Int(nullable: false));
            CreateIndex("dbo.RutaIncidencia", "RutaId");
            AddForeignKey("dbo.RutaIncidencia", "RutaId", "dbo.Ruta", "Id");
            DropColumn("dbo.RutaIncidencia", "ChoferId");
            DropColumn("dbo.RutaIncidencia", "VehiculoId");
            DropTable("dbo.PlanMantenimiento");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.PlanMantenimiento",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        TipoMantenimiento = c.String(),
                        TipoVehiculo = c.String(),
                        Kilometraje = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Observaciones = c.String(),
                        Estado = c.String(),
                        UltimoUsuario = c.String(),
                        UltimaFechaModif = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.RutaIncidencia", "VehiculoId", c => c.Int(nullable: false));
            AddColumn("dbo.RutaIncidencia", "ChoferId", c => c.Int(nullable: false));
            DropForeignKey("dbo.RutaIncidencia", "RutaId", "dbo.Ruta");
            DropIndex("dbo.RutaIncidencia", new[] { "RutaId" });
            DropColumn("dbo.RutaIncidencia", "RutaId");
            DropColumn("dbo.MantenimientoVehiculo", "FechaRegistro");
            DropColumn("dbo.Vehiculo", "FechaVencimientoSoat");
            DropColumn("dbo.Consumo", "PrecioUnitario");
            DropColumn("dbo.Consumo", "Cantidad");
            CreateIndex("dbo.RutaIncidencia", "VehiculoId");
            CreateIndex("dbo.RutaIncidencia", "ChoferId");
            AddForeignKey("dbo.RutaIncidencia", "VehiculoId", "dbo.Vehiculo", "Id");
            AddForeignKey("dbo.RutaIncidencia", "ChoferId", "dbo.Chofer", "Id");
        }
    }
}
