﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ventas.Repositorio.Entidades;

namespace Ventas.Repositorio
{
    public class OlimpoContext : DbContext
    {
        public OlimpoContext():base("name=OlimpoDB")
        {

        }
        //public DbSet<Asistente> Asistentes { get; set; }
        //public DbSet<Usuario> Usuarios { get; set; }
        //public DbSet<Evento>  Eventos { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Direccion> Direcciones { get; set; }
        public DbSet<Venta> Ventas { get; set; }
        public DbSet<VentaDetalle> VentasDetalle { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
    }
}
