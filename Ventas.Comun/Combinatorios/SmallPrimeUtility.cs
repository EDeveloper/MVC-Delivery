using System.Collections;
using System.Collections.Generic;

namespace Ventas.Comun.Combinatorios {
    /// <summary>
    /// Clase de que mantiene una tabla de numeros primos y provee
    /// una implementacion de Algoritmos de Factorizacion de Numeros Primos
    /// Permite calculo de permutaciones de un conjunto de 2^31 elementos
    /// </summary>
    public class UtilidadPrimos {
        private UtilidadPrimos() {
            ;
        }

        public static List<int> Factores(int i) {
            int indicePrimo = 0;
            int primo = TablaPrimos[indicePrimo];
            List<int> factores = new List<int>();
            while(i > 1) {
                if(i % primo == 0) {
                    factores.Add(primo);
                    i /= primo;
                }
                else {
                    ++indicePrimo;
                    primo = TablaPrimos[indicePrimo];
                }
            }
            return factores;
        }

        public static List<int> MultiplesFactoresPrimos(IList<int> lhs, IList<int> rhs) {
            List<int> producto = new List<int>();
            foreach(int primo in lhs) {
                producto.Add(primo);
            }
            foreach(int prime in rhs) {
                producto.Add(prime);
            }
            producto.Sort();
            return producto;
        }

        public static List<int> DivideFactoresPrimos(IList<int> numerador, IList<int> denominador) {
            List<int> producto = new List<int>();
            foreach(int primo in numerador) {
                producto.Add(primo);
            }
            foreach(int prime in denominador) {
                producto.Remove(prime);
            }
            return producto;
        }

        public static long EvaluaFactoresPrimos(IList<int> valor) {
            long acumulador = 1;
            foreach(int primo in valor) {
                acumulador *= primo;
            }
            return acumulador;
        }

        static UtilidadPrimos() {
            CalculaPrimos();
        }

        private static void CalculaPrimos() {
            BitArray semilla = new BitArray(65536, true);
            for(int posiblePrimo = 2; posiblePrimo <= 256; ++posiblePrimo) {
                if(semilla[posiblePrimo] == true) {
                    // It is prime, so remove all future factors...
                    for(int noPrimo = 2 * posiblePrimo; noPrimo < 65536; noPrimo += posiblePrimo) {
                        semilla[noPrimo] = false;
                    }
                }
            }
            misPrimos = new List<int>();
            for(int i = 2; i < 65536; ++i) {
                if(semilla[i] == true) {
                    misPrimos.Add(i);
                }
            }

        }

        public static IList<int> TablaPrimos {
            get {
                return misPrimos;
            }
        }

        private static List<int> misPrimos = new List<int>();


        public static IEnumerable<int> numerador { get; set; }
    }
}
