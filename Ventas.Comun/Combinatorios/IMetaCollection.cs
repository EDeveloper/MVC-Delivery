using System.Collections.Generic;

namespace Ventas.Comun.Combinatorios
{
    /// <summary>
    /// Interface para las Permutaciones y/o Combinaciones la cual se basa en la lista de definicion
    /// Nos permitira recorrer las permutaciones y combinaciones generadas
    /// </summary>
    /// <typeparam name="T">Tipo de los elementos de la coleccion .</typeparam>
    interface IMetaColeccion<T> : IEnumerable<IList<T>>
    {
        long Count { get; }

        GenerateOption Tipo { get; }

        int MaxIndex { get; }

        int MinIndex { get; }
    }

}
