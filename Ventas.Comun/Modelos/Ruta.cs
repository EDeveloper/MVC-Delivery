﻿namespace Ventas.Comun.Modelos
{
    public class Ruta<T>
    {
        public T Origen { get; set; }
        public T Destino { get; set; }
        public decimal Distancia { get; set; }
    }
}