﻿using Ventas.Repositorio.Entidades;

namespace Ventas.Comun.Modelos
{
    // clase que contiene el posible cliente a visitar
    public class Visitas
    {
        public int Id { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }

        public int Codigo { get; set; }

        public string Nombre { get; set; }
        public int DireccionId { get; set; }

        public Direccion Direccion { get; set; }
    }
}