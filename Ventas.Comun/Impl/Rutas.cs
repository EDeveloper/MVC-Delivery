﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ventas.Comun.Modelos;
using Ventas.Comun.Viajero;
using Ventas.Repositorio.Entidades;

namespace Ventas.Comun.Impl
{
    public class Rutas
    {
        public Location Origen { get; private set; }

        public Rutas()
        {
            Origen = new Location(0, Properties.Settings.Default.OrigenLatitud, Properties.Settings.Default.OrigenLongitud);
        }
        public List<Venta> Entregas(List<Venta> entregas, int capacidadMaxima)
        {
            if (entregas.Count<=0) return entregas;
            var algoritmoMochila = new Mochila();
            var totalEntregar = 0M;
            var aEntregar= algoritmoMochila.CargaALLevar(entregas, capacidadMaxima, out totalEntregar);

            var locaciones =
                aEntregar.Select(
                    x =>
                        new Location(x.Id, Convert.ToDouble(x.VentaDireccion.Latitud),
                            Convert.ToDouble(x.VentaDireccion.Longitud)));

            List<Location> solucion = ObtenerSolucion(locaciones.ToArray());

            var ruta = new List<Venta>();
            solucion.ForEach(x =>
            {
                ruta.Add(entregas.FirstOrDefault(y => y.Id == x.Id));
            });
            return ruta.Where(x=>x.Id!=0).ToList();
        }

        private List<Location> ObtenerSolucion(Location[] locations)
        {
            var algoritmo = new TravellingSalesmanAlgorithm(Origen, locations, locations.Length * 8);
            var iteraciones = 0;
            var nuevaSolucion = algoritmo.GetBestSolutionSoFar().ToArray();
            Location[] mejorSolucion;
            algoritmo.MustMutateFailedCrossovers = true;
            algoritmo.MustDoCrossovers = false;
            do
            {
                
                mejorSolucion = nuevaSolucion;
                algoritmo.Reproduce();
                algoritmo.MutateDuplicates();
                nuevaSolucion = algoritmo.GetBestSolutionSoFar().ToArray();
                if (locations.Length <= 5 && iteraciones > 100) break;
                iteraciones++;
            } while (!nuevaSolucion.SequenceEqual(mejorSolucion));

            return mejorSolucion.ToList();
        }



        public List<Visitas> Entregas(List<Visitas> clientesAVisitar)
        {
            if (clientesAVisitar.Count <= 0) return clientesAVisitar;
            var locaciones =
                clientesAVisitar.Select(
                    x =>
                        new Location(x.Id, Convert.ToDouble(x.Latitud),
                            Convert.ToDouble(x.Longitud)));

            List<Location> solucion = ObtenerSolucion(locaciones.ToArray());

            var ruta = new List<Visitas>();
            solucion.ForEach(x =>
            {
                ruta.Add(clientesAVisitar.FirstOrDefault(y => y.Id == x.Id));
            });
            return ruta;            
        }
    }
}