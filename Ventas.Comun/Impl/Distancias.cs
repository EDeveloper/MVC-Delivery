﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ventas.Comun.Modelos;
using Ventas.Repositorio.Entidades;

namespace Ventas.Comun.Impl
{
    public sealed class Distancias
    {
        private readonly IList<Direccion> Direcciones;
        public Distancias(IList<Direccion> direcciones)
        {
            this.Direcciones = direcciones;
        }

        public IEnumerable<Ruta<Direccion>> ObtieneRutas()
        {
            var combinaciones = new Combinatorios.Combinations<Direccion>(Direcciones, 2);
            foreach (var combination in combinaciones)
            {
                yield return new Ruta<Direccion>
                {
                    Origen = combination[0],
                    Destino = combination[1]
                };
            }
        }




    }
}
