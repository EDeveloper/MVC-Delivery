﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ventas.Repositorio.Entidades;

namespace Ventas.Comun.Impl
{
    public sealed class Mochila
    {
        /// <summary>
        /// Obtiene la lista de ventas a llevar por un cambio
        /// </summary>
        /// <param name="items">Ventas a entregar del dia</param>
        /// <param name="capacidad">el es valor maximo de carga (7000 soles) para tomar en cuenta los decimales</param>
        /// <param name="totalValue">cantidad de carga que se llevara</param>
        /// <returns></returns>
        public IList<Venta> CargaALLevar(IList<Venta> items, int capacidad, out decimal totalValue)
        {

            decimal[,] valor = new decimal[items.Count + 1, capacidad + 1];
            bool[,] aCargar = new bool[items.Count + 1, capacidad + 1];

            for (int i = 1; i <= items.Count; i++)
            {
                Venta actualItem = items[i - 1];
                for (int espacio = 1; espacio <= capacidad; espacio++)
                {
                    if (espacio >= (int)actualItem.PesoTotal)
                    {
                        var remainingSpace = espacio - (int)actualItem.PesoTotal;
                        var  remainingSpaceValue = 0M;
                        if (remainingSpace > 0)
                        {
                            remainingSpaceValue = valor[i - 1, remainingSpace];
                        }
                        var currentItemTotalValue = actualItem.VentaDetalle.Sum(x=>x.Cantidad) + remainingSpaceValue;
                        if (currentItemTotalValue > valor[i - 1, espacio])
                        {
                            aCargar[i, espacio] = true;
                            valor[i, espacio] = currentItemTotalValue;
                        }
                        else
                        {
                            aCargar[i, espacio] = false;
                            valor[i, espacio] = valor[i - 1, espacio];
                        }
                    }
                }
            }

            var itemsACargar = new List<Venta>();

            int espacioDisponible = capacidad;
            int item = items.Count;
            while (item > 0)
            {
                if (aCargar[item, espacioDisponible])
                {
                    itemsACargar.Add(items[item - 1]);
                    espacioDisponible = espacioDisponible - (int)(items[item - 1].PesoTotal);
                }
                item--;
            }

            totalValue = valor[items.Count, capacidad];
            return itemsACargar;
        }

    }

}


